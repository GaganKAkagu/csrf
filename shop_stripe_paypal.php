<?php 

    require('core/init.php');

    //Calling the get price function (located in the functions directory)
    require_once('get-price-and-time.php');

    //initializing required classes
    $users            = new User();
    $user_addresses   = new User_Address();
    $user_payments    = new User_Payment();

    $products         = new Product();
    $product_options  = new Product_Option();
    $product_images   = new Product_Image();

    $brands           = new Brand();
    $brand_deliveries = new Brand_Delivery();


    /*if(!$users->isLoggedIn()){
        Redirect::to(ROOT_URL_SECURE.'login.php?status=user-login-required');
    }*/


    $product_id  =  100;


    //fetching or necessary data if available else leave it empty for users to fill
    $user_address = $user_addresses->find('user_address_user_id', 251);
    $user_payment = $user_payments->find('user_payment_user_id', 251);


    //checking if user payment details are already available and if yes, retreive them
    /*if($user_payment){


        //Intialize Strip Autoloader
        require_once('apis/stripe/vendor/autoload.php');

        //Set Stripe APi Private Key
        require_once ('apis/stripe/config.php');

        //retreive data from 
        $customer_payment = \Stripe\Customer::retrieve($user_payment[0]->user_payment_card_id);

        $customer_payment_json    = $customer_payment->__toJSON();
        $customer_payment_decoded = json_decode($customer_payment_json);

        $card_brand = $customer_payment_decoded->sources->data[0]->brand;
        $card_last4 = $customer_payment_decoded->sources->data[0]->last4;

        $user_payment_data = 'true';


    }*/


    //checking if user address and payments details are available and if yes, show the summary tab
    if($user_address && $user_payment){
        $user_information = true;
    }


    //check if data has been sent for processing from the shop page
    //if($_SESSION['product_id'] && $_SESSION['product_option'] && $_SESSION['product_quantity'] && $_SESSION['time']){

        //define all variables to local variables
        /*$product_id         = $_SESSION['product_id'];
        $product_option     = $_SESSION['product_option'];
        $product_quantity   = $_SESSION['product_quantity'];
        $action             = $_SESSION['action'];
        $time               = $_SESSION['time'];
        $images             = $product_images->find('product_image_product_id', $product_id);*/

        // static values
        //$product_id         = $_SESSION['product_id'];
        $product_option     = 10;
        $product_quantity   = 1;
        $action             = 'buy';
        //$time               = $_SESSION['time'];
        $images             = $product_images->find('product_image_product_id', $product_id);


        //Product Price is the actual product price obtained by using the price drop algorithm

        //fetching product data and product price when a user clicks on the bid now button
        $product        = $products->find('product_id', $product_id);
        
        if($action == 'bid'){
            $product_price  = getprice($product[0]->product_bid_start_date, $product[0]->product_bid_start_time, $product[0]->product_bid_end_date, $product[0]->product_bid_end_time, $product[0]->product_price_start, $product[0]->product_price_end, $product[0]->product_drop_frequency, $time);//get product price using timer input
        }else if($action == 'buy'){
            $product_price  = $product[0]->product_rrp;//get product price using timer input
        }


        //assigning product name to a session variable to carry forward to the thank you page
        //$_SESSION['product_name']         = ucwords($product[0]->product_name);
        $product_name                     = ucwords($product[0]->product_name);


        //fetching product_option data to swrite on the invoice like color and size, etc.
        $product_option      = $product_options->find('product_options_id', $product_option);
        $product_option_name = $product_option[0]->product_options_product_option;

        $brand          = $brands->find('brand_id', $product[0]->product_brand_id);
        $brand_name     = ucwords($brand[0]->brand_name);

        //$brand_delivery = $brand_deliveries->find('brand_delivery_brand_id', $brand[0]->brand_id);
        $brand_delivery = $brand_deliveries->find_order($brand[0]->brand_id);
     

        //unit product price
        $product_price_per_unit = $product_price;


        //calculating the product price with the quantity accounted for
        $product_price  = $product_price_per_unit * $product_quantity;
    /*}else{
        //redirect users off this page here
        echo 'Session data not available.';
    }*/



?>

<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Akagu - Shop</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php
            //including common stylesheets and favicons
            include_once('include/includes_header.php');
        ?>

        <style type="text/css">
            .button-disabled{
                pointer-events: none;
                cursor: default;
            }
        </style>

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <script type="text/javascript">
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
              'event': 'addToCart',
              'ecommerce': {
                'currencyCode': 'AUD',
                'add': {                                // 'add' actionFieldObject measures.
                  'products': [{                        //  adding a product to a shopping cart.
                    'name': "<?php echo ucwords($product[0]->product_name); ?>",
                    'id': "<?php echo $product_id; ?>",
                    'price': "<?php echo $product_price; ?>",
                    'brand': "<?php echo ucwords($brand_name); ?>",
                    'category': "<?php echo ucwords($product[0]->product_tags); ?>",
                    'variant': "<?php echo ucwords($product_option_name); ?>",
                    'quantity': "<?php echo $product_quantity; ?>"
                   }]
                }
              }
            });
        </script>

        <?php include ('views/templates/header-slim.php'); ?>
        
        <main>
            <section class="section-xlarge mobile-padding-remove">
                <div class="uk-grid mobile-uk-grid-fix">
                    <div id="wrapper" class="uk-width-small-1-1 uk-width-large-2-6 uk-width-xlarge-1-4 max-width uk-container-center uk-text-center section-border transition">
                            
                            <div class="uk-tab-center mobile-shop-navigation-separator" data-uk-sticky="{media: '(max-width: 480px)'}">
                                <!-- This is the container of the toggling elements -->
                                <ul class="uk-tab" data-uk-switcher="{connect:'#my-id', animation: 'slide-horizontal' <?php if($user_information){echo ',active: 2';} ?>}">
                                    <li id="tab-delivery" class="button-disabled">
                                        <a href="">
                                            <div class="uk-margin-small-bottom"><i class="icon-delivery-small uk-hidden-small uk-icon-small"></i></div>
                                            <div>Delivery</div>
                                        </a>
                                    </li>

                                    <li id="tab-payment" class="uk-margin-left uk-margin-right button-disabled">
                                        <a href="">
                                            <div class="uk-margin-small-bottom"><i class="uk-icon-credit-card uk-hidden-small uk-icon-small"></i></div>
                                            <div>Payment</div>
                                        </a>
                                    </li>

                                    <li id="tab-summary" class="button-disabled">
                                        <a href="">
                                            <div class="uk-margin-small-bottom"><i class="icon-list uk-hidden-small uk-icon-small"></i></div>
                                            <div>Summary</div>
                                        </a>
                                    </li>
                                </ul>
                            </div>


                            <div class="section-large mobile-section-large mobile-block">

                                
                                <!-- This is the container of the content items -->
                                <ul id="my-id" class="uk-switcher">
                                    <li>
                                        
                                        <form id="form-delivery" class="uk-form" method="post" action="">
                                    
                                            <input id="address" name="address" type="text" class="uk-form-row uk-width-small-1-1" placeholder="Address*" value="<?php echo $user_address[0]->user_address_user_address; ?>"></input>

                                            <input id="suburb" name="suburb" type="text" class="uk-form-row uk-width-small-1-1" placeholder="Suburb*" value="<?php echo $user_address[0]->user_address_user_suburb; ?>"></input>

                                            <input id="state" name="state" type="text" class="uk-form-row uk-width-small-1-1" placeholder="State/Province" value="<?php echo $user_address[0]->user_address_user_state; ?>"></input>

                                            <select id="country" name="country" class="uk-form-row uk-width-small-1-1">
                                                <option value="0">Select your Country</option>
                                                <?php include('resources/country-list.php'); ?>
                                            </select>

                                            <input id="postal_code" name="postal_code" type="text" class="uk-form-row uk-width-small-1-1" placeholder="Postal Code*" value="<?php echo $user_address[0]->user_address_user_postal_code; ?>"></input>

                                            
                                            <a href="" id="button-delivery" class="uk-button uk-button-large button-secondary-solid uk-width-small-1-1 uk-margin-top"> 
                                                <span class="content-button-delivery">
                                                    Next <i class="uk-margin-left uk-icon-arrow-right"></i>
                                                </span>
                                                <img src="assets/images/loading-light.svg" width="35" class="loading-delivery uk-hidden">
                                            </a>

                                            <?php
                                                if($user_address){
                                                    echo '<div class="uk-text-left uk-margin-top uk-margin-bottom uk-float-right"><a href="" data-uk-switcher-item="1">Use the same delivery address <i class="icon-right-open-mini"></i></a></div>';
                                                }
                                            ?>
                                            

                                        </form>

                                    </li>

                                    <li>
                                        
                                        <form id="form-payment" class="uk-form" method="post" action="">
                                    
                                            <div>
                                                <input id="checkout_card_number" name="credit_card_number" type="text" size="20" pattern="[0-9]*" class="uk-form-row uk-width-small-1-1 stripe_card_number" placeholder="* Credit Card Number" autocomplete="off" required="required" data-stripe="number"></input>
                                            </div>    

                                                <ul class="card_logos uk-margin-small-top">
                                                    <li class="card_visa">Visa</li>
                                                    <li class="card_mastercard">Mastercard</li>
                                                    <li class="card_amex">American Express</li>
                                                    <li class="card_discover">Discover</li>
                                                </ul>

                                            <div class="uk-clearfix">
                                                <div class="uk-float-left uk-width-small-5-10 uk-width-medium-6-10">
                                                    <select id="expiry_month" name="expiry_month" class="uk-width-1-1" data-stripe="exp-month">
                                                        <option value="">Expiry Month</option>
                                                        <option value="1">01 - January</option>
                                                        <option value="2">02 - February</option>
                                                        <option value="3">03 - March</option>
                                                        <option value="4">04 - April</option>
                                                        <option value="5">05 - May</option>
                                                        <option value="6">06 - June</option>
                                                        <option value="7">07 - July</option>
                                                        <option value="8">08 - August</option>
                                                        <option value="9">09 - September</option>
                                                        <option value="10">10 - October</option>
                                                        <option value="11">11 - November</option>
                                                        <option value="12">12 - December</option>
                                                    </select>
                                                </div>

                                                <div class="uk-float-right">
                                                    <select id="expiry_year" name="expiry_year" class="uk-width-1-1" data-stripe="exp-year">
                                                        <option value="">Expiry Year</option>
                                                        <?php
                                                            for($y = date(Y); $y < date(Y) + 10; $y++){
                                                                echo '<option value="'.$y.'">'.$y.'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="uk-text-left uk-position-relative">    
                                                <div class="uk-grid">    
                                                    <div class="uk-width-small-1-1">    
                                                        <span class="hint" data-uk-tooltip title='For Visa, Master and Discover (left), the 3 digits on the <strong>back</strong> of your card. <br/><br/> For American Express (right), the 4 digits on the <strong>front</strong> of your card.<br/><br/><img src="assets/images/hint-credit-card.png" class="uk-text-center" /> '>
                                                            <i class="icon-help uk-icon-small"></i>
                                                        </span>
                                                        <input id="cvv" name="credit_card_cvv2" type="text" pattern="[0-9]*" class="uk-form-row uk-width-small-1-1 uk-float-left" placeholder="* Security Code (CVV/CVC)" data-stripe="cvc">
                                                        
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="checkbox uk-text-left">    
                                                <div class="uk-grid">  
                                                    <div class="uk-width-small-1-10 uk-width-medium-1-10">
                                                        <input type="checkbox" id="store-credit-card" name="store-credit-card" class="uk-margin-right" checked />
                                                    </div> 
                                                    <div class="uk-width-small-8-10 uk-width-medium-9-10">
                                                        <label for="store-credit-card">Store your credit card securely for future purchases</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="error-message-card" class="error"></div>

                                            <a href="" id="button-payment" class="uk-button uk-button-large button-secondary-solid uk-width-small-1-1 uk-margin-top">

                                                <span class="content-button-delivery">
                                                    Next <i class="uk-margin-left uk-icon-arrow-right"></i>
                                                </span>
                                                <img src="assets/images/loading-light.svg" width="35" class="loading-payment uk-hidden">
                                                </a>

                                            </a>

                                            <div class="uk-text-left uk-margin-top uk-margin-bottom uk-float-left"><a href="" data-uk-switcher-item="0"><i class="icon-left-open-mini"></i>Back to delivery</a></div>

                                            <?php
                                                if($user_payment){
                                                    echo '<div class="uk-text-left uk-margin-top uk-margin-bottom uk-float-right"><a href="" data-uk-switcher-item="2">I will use the same card<i class="icon-right-open-mini"></i></a> </div>';
                                                }
                                            ?>
                                            

                                        </form>

                                    </li>
                                    <li>
                                        <form id="form-summary" class="uk-form" method="post" action="">
                                    
                                            <div class="uk-grid uk-grid-large">
                                                <div class="uk-width-small-1-1 uk-width-medium-1-2">
                                                    <div class="uk-clearfix">
                                                        <span class="text-column-title uk-float-left">Delivery</span>
                                                        <span class="uk-float-right"><a href=""  data-uk-switcher-item="0" class="link-dark-secondary">Ship to a different address?</a></span>
                                                    </div>
                                                    <div id="wrapper-address" class="uk-text-left uk-margin-top">
                                                        
                                                        <?php
                                                            echo $user_address[0]->user_address_user_address.' <br/> '.$user_address[0]->user_address_user_suburb.', '.$user_address[0]->user_address_user_state.', '.$user_address[0]->user_address_user_postal_code.' <br/> '.$user_address[0]->user_address_user_country;
                                                        ?>

                                                    </div>
                                                    <div class="uk-text-left uk-margin-top">
                                                        <label for="delivery_option"><strong>Choose a delivery method</strong></label>
                                                        <select id="delivery_option" name="delivery_option" class="uk-width-small-1-1" style="margin-top: 5px;">
                                                        <?php
                                                            foreach($brand_delivery as $delivery){
                                                        ?>        
                                                                <option value="<?php echo $delivery->brand_delivery_option_value; ?>"><?php echo ucwords($delivery->brand_delivery_option); ?>
                                                                </option>

                                                        <?php
                                                            }
                                                        ?>

                                                        </select>
                                                    </div>

                                                    <hr>

                                                    <div class="uk-clearfix">
                                                        <span class="text-column-title uk-float-left">Payment</span>
                                                        <span class="uk-float-right"><a href=""  data-uk-switcher-item="1" class="link-dark-secondary">Pay using a different card?</a></span>
                                                    </div>
                                                    <div class="uk-text-left uk-margin-top">
                                                        <!-- 
                                                            User Credit Card Icons  brand -> MasterCard / Visa / AmericanExpress  / Discover
                                                        -->
                                                        <?php
                                                            if($card_brand == 'Visa'){

                                                                $card_type = 'visa';

                                                            }else if($card_brand == 'MasterCard'){

                                                                $card_type = 'mastercard';

                                                            }else if($card_brand == 'American Express'){

                                                                $card_type = 'amex';

                                                            }else if($card_brand == 'Discover'){

                                                                $card_type = 'discover';
                                                            }
                                                        ?>
                                                        <ul id="wrapper-card-logos" class="card_logos uk-margin-small-right uk-float-left <?php echo 'is_'.$card_type; ?>">
                                                            <li class="<?php echo 'card_'.$card_type; ?>"></li>
                                                        </ul>

                                                        <div class="uk-float-left" style="font-size: 8px;letter-spacing: -5px;">
                                                            <span class="icon-circle"></span>
                                                            <span class="icon-circle"></span>
                                                            <span class="icon-circle"></span>
                                                            <span class="icon-circle uk-margin-small-right"></span>

                                                            <span class="icon-circle"></span>
                                                            <span class="icon-circle"></span>
                                                            <span class="icon-circle"></span>
                                                            <span class="icon-circle uk-margin-small-right"></span>

                                                            <span class="icon-circle"></span>
                                                            <span class="icon-circle"></span>
                                                            <span class="icon-circle"></span>
                                                            <span class="icon-circle uk-margin-small-right"></span>
                                                        </div>
                                                         <?php echo '<span id="wrapper-card-last4" class="uk-margin-small-left">'.$customer_payment_decoded->sources->data[0]->last4.'</span>'; ?>
                                                        
                                                    </div>

                                                    <hr>

                                                    <div class="uk-clearfix">
                                                        <div class="uk-text-left">Have a coupon code? <a href="javascript:void();" class="link-secondary" data-uk-toggle="{target:'#coupon-code-wrapper' , animation:'uk-animation-fade'}">Apply Now</a></div>
                                                    </div>
                                                    <div id="coupon-code-wrapper" class="uk-text-left uk-margin-small-top uk-width-small-1-1 uk-hidden">
                                                        
                                                            <input id="coupon_code" type="text" name="coupon_code" class="uk-form-row uk-width-small-1-1 uk-width-medium-8-10 uk-width-large-7-10 uk-margin-top-remove uk-margin-small-right" placeholder="Enter your coupon code"></input>
                                                            <button id="button-coupon-code" class="uk-button button-secondary-outline uk-margin-top-remove uk-width-small-1-1 uk-width-medium-2-10 mobile-margin-small-top">APPLY</button>
                                                            <div class="coupon-code-response text-secondary"></div>
                                                        
                                                    </div>

                                                    <hr class="uk-visible-small">

                                                </div>

                                                <div class="uk-width-small-1-1 uk-width-medium-1-2">
                                                    <div class="section-border">
                                                        <div class="section-medium">

                                                            <div class="uk-grid">
                                                                <div class="uk-width-small-1-2 uk-width-medium-4-10 uk-width-large-4-10">
                                                                    <img src="<?php echo 'administrator/functions/uploadedimages/thumbs/'.$images[0]->product_image_product_name; ?>" class="section-padding">
                                                                </div>
                                                                <div class="uk-width-small-1-2 uk-width-medium-6-10 uk-width-large-6-10 uk-margin-top uk-text-left">
                                                                    <div class="text-product-name uk-margin"> <?php echo $product_name; ?></div>
                                                                    <div class=""><strong>Option:</strong> <?php echo ucwords($product_option_name); ?></div>
                                                                    <div class=""><strong>Quantity:</strong> <?php echo $product_quantity; ?></div>

                                                                    <input type="hidden" name="item_name" value="Shirt">
                                                                    
                                                                    <input type="hidden" name="amount" value="<?php echo $product_price; ?>">


                                                                    <div class=""><strong>Price:</strong> $AUD 
                                                                        <span id="display_product_price_discounted" class="uk-hidden"></span>
                                                                        <span id="display_product_price" class=""><?php echo sprintf('%0.2f', $product_price); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            

                                                        </div>
                                                    </div>
                                                            
                                                        <div class="summary">
                                                        

                                                            <div class="section-medium background-dark-primary">
                                                                <div class="uk-margin-bottom">
                                                                    <div class="uk-grid text-light uk-grid-collapse">

                                                                        <div class="uk-width-small-1-1 uk-width-medium-1-10 uk-width-large-1-10">
                                                                            &nbsp;
                                                                        </div>

                                                                        <div class="uk-width-small-1-2 uk-width-medium-4-10 uk-width-large-4-10 uk-text-left">
                                                                            <div class="text-summary mobile-margin-small-left">Product Price</div>
                                                                            <div class="text-summary mobile-margin-small-left">Shipping Cost</div>
                                                                            <div class="text-section-title text-light uk-margin-top mobile-margin-small-left"><strong>Total: </strong></div>
                                                                        </div>

                                                                        <?php
                                                                            $total_price = $product_price + $brand_delivery[0]->brand_delivery_option_value;
                                                                        ?>

                                                                        <div class="uk-width-small-1-2 uk-width-medium-4-10 uk-width-large-5-10 uk-text-left">
                                                                            <div class="text-summary">$ AUD <span id="display_product_price_final"><?php echo sprintf('%0.2f', $product_price); ?></span></div>
                                                                            <div class="text-summary">$ AUD <span id="display_delivery_cost"><?php echo sprintf('%0.2f', $brand_delivery[0]->brand_delivery_option_value); ?></span></div>
                                                                            <div class="text-section-title text-light uk-margin-top"><strong>$ AUD <span id="display_total"><?php echo sprintf('%0.2f', $total_price); ?></span></strong></div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="transaction-response uk-margin-small-top"></div>

                                                        <button id="button-summary" class="uk-button uk-button-large button-tertiary-solid uk-width-small-1-1 uk-hidden-small" style="margin-top: 10px;">
                                                        
                                                            <span class="content-button-confirm">
                                                                <i class="icon-lock uk-icon-small"></i>
                                                                CONFIRM & PAY
                                                            </span>
                                                            <img src="assets/images/loading-light.svg" width="35" class="loading-summary uk-hidden">
                                                        </button>


                                                        <button id="mobile-button-summary" class="uk-button uk-button-large mobile-button-tertiary-solid uk-width-small-1-1 uk-visible-small" style="margin-top: 10px;">
                                                            
                                                            <span class="content-button-confirm">
                                                                <i class="icon-lock"></i>
                                                                CONFIRM & PAY ($ AUD <span id="mobile_display_total"><?php echo sprintf('%0.2f', $product_price); ?></span>)
                                                            </span>
                                                            <img src="assets/images/loading-light.svg" width="35" class="loading-summary uk-hidden">
                                                         </button>

                                                         

                                                        <div class="uk-text-center uk-margin-top uk-align-center">
                                                            
                                                            <img src="<?php echo ROOT_STATIC; ?>images/icon-security-comodo.png" width="90">
                                                        </div>
                                                    
                                                    
                                                </div>
                                            </div>
                                            

                                        </form>
                                    </li>


                                </ul>

                            </div>
                    </div>                  
                </div>
            </section>

            <section class="section-large background-light-primary">

                                       
                                        <div class="uk-grid uk-container-center" >
                                                

                                            <div class="uk-width-small-1-1 uk-width-medium-8-10 uk-width-large-8-10 uk-container-center uk-text-center max-width">

                                                <div class="uk-grid">
                                                    <div class="uk-width-small-1-1 uk-width-medium-1-3 uk-width-large-1-3 uk-margin-bottom mobile-padding-remove mobile-block">
                                                        <h3 class="text-column-title">Security</h3>
                                                        <div class="icon-lock-large uk-align-center"></div>
                                                        
                                                        <p>
                                                            Every transaction is protected by Akagu through [SSL], [Stripe], [Norton Secured] so you can shop in comfort.
                                                        </p>
                                                    </div>
                                                    <div class="uk-width-small-1-1 uk-width-medium-1-3 uk-width-large-1-3 uk-margin-bottom mobile-padding-remove mobile-block">
                                                        <h3 class="text-column-title">Returns & Shipping</h3>
                                                        <div class="icon-return-large uk-align-center"></div>
                                                        
                                                        <p>
                                                            Returns and shipping follows the individual designer label's' returns and shipping policies. Please visit the designer's brand page for more details!
                                                        </p>
                                                    </div>
                                                    <div class="uk-width-small-1-1 uk-width-medium-1-3 uk-width-large-1-3 uk-margin-bottom mobile-padding-remove mobile-block">
                                                        <h3 class="text-column-title">Support</h3>
                                                        <div class="icon-support-large uk-align-center"></div>
                                                        
                                                        <p>
                                                            Akagu technical support will respond to you within 24 hours. Any enquiries related to returns, refunds, or shipping should be directed to the designer label.
                                                        </p>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                    
                                </section>


            <?php include('views/templates/footer-slim.php'); ?>


        </main>

        <script>window.jQuery || document.write('<script src="<?php echo ROOT_STATIC; ?>js/vendor/jquery-1.12.3.min.js"><\/script>')</script>

        <script src="<?php echo ROOT_STATIC; ?>js/main.js"></script>
        <script src="<?php echo ROOT_STATIC; ?>js/uikit.js"></script>
        <script src="<?php echo ROOT_STATIC; ?>js/components/tooltip.js"></script>
        <script src="<?php echo ROOT_STATIC; ?>js/components/sticky.js"></script>

        <script src="<?php echo ROOT_STATIC; ?>libraries/jquery.validate-credit-card.js"></script>
        
        <script src="<?php echo ROOT_STATIC; ?>libraries/jquery.validate.min.js"></script>
        <script src="<?php echo ROOT_STATIC; ?>js/validations/validate-form-delivery.js"></script>
        <script src="<?php echo ROOT_STATIC; ?>js/validations/validate-form-payment.js"></script>

        <script src="<?php echo ROOT_STATIC; ?>js/plugins.js"></script>

        <!-- The required Stripe lib -->
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>


        <script type="text/javascript">
            $(document).ready(function(){
                $('#checkout_card_number').creditCardTypeDetector({ 'credit_card_logos' : '.card_logos' });
            });
        </script>


        <script type="text/javascript">

            $('[data-uk-switcher]').on('show.uk.switcher', function(event, area){
                if(area[0].id == "tab-summary"){
                    $("#wrapper").addClass("uk-width-large-4-6 uk-width-xlarge-5-10").removeClass("uk-width-large-2-6 uk-width-xlarge-1-4");               
                }else{
                    $("#wrapper").addClass("uk-width-large-2-6 uk-width-xlarge-1-4").removeClass("uk-width-large-4-6 uk-width-xlarge-5-10");
                }
            });

        </script>


        <script type="text/javascript">
             /* A function to handle a click leading to a checkout option selection.*/
            function onCheckoutOption(step, checkoutOption) {
              //window.dataLayer = window.dataLayer || []
              dataLayer.push({
                'event': 'checkoutOption',
                'ecommerce': {
                  'checkout_option': {
                    'actionField': {'step': step, 'option': checkoutOption}
                  }
                }
              });
            }
        </script>


        <script type="text/javascript">

                var delivery = false;
                var payment  = false;

                $('#button-delivery').click(function(){
                
                    var form = $('#form-delivery');                        
                    event.preventDefault();

                    if(form.valid() == true){

                        delivery = true;

                        if(delivery == true){
                            //form processing via ajax to store delivery data

                            $('.loading-delivery').removeClass('uk-hidden');
                            $('#button-delivery').addClass('button-secondary-solid-disabled');

                            var form_data = form.serialize();

                            //Ajax request to the file that does the processing
                            var request = $.ajax({
                                url: "functions/ajax-delivery.php",
                                method: "POST",
                                data: { data : form_data },
                                dataType: "json"
                            });
                                     
                            request.done(function(data) {

                                if(data.result == true){
                                    $('.loading-delivery').addClass('uk-hidden');
                                    $('#button-delivery').removeClass('button-secondary-solid-disabled').attr("data-uk-switcher-item", "next").unbind().click();
                                    $('#wrapper-address').html(data.information);
                                }

                                
                                /**
                                 * A function to handle a click leading to a checkout option selection.
                                 */
                                onCheckoutOption('Delivery', $('#country').val());
                                dataLayer.push({‘checkoutProcess’: '1'});

                               
                                    
                            });
                                     
                            request.fail(function(jqXHR, textStatus) {
                                console.log("Error: "+textStatus);
                            });

                            request.always(function(jqXHR, textStatus, errorThrown){
                                console.log("Complete: "+textStatus);
                            })

                            
                        }else{
                            $('#button-delivery').removeAttr("data-uk-switcher-item", "next");
                            //console.log("Stay");
                        }

                    }else{
                        $('#button-delivery').removeAttr("data-uk-switcher-item", "next");
                        delivery == false;
                    }
                       
                });




                //Stripe Payment

                // This identifies your website in the createToken call below
                //Stripe.setPublishableKey('pk_live_58VWexg4lDUZFmomWnIIWnwQ');
                Stripe.setPublishableKey('pk_test_qe6qyJhnMkn6fi7KMJWu04Sl');


                $('#button-payment').click(function(){

                    var form = $('#form-payment');                      
                    event.preventDefault();

                        if(form.valid() == true){

                            payment = true;

                            if(payment == true){

                                //form processing via ajax to store delivery data
                                $('.loading-payment').removeClass('uk-hidden');
                                $('#button-payment').addClass('button-secondary-solid-disabled').prop('disabled', true);

                                //Creating the token
                                Stripe.card.createToken(form, stripeResponseHandler);

                                
                            }else{
                                $('#button-payment').removeAttr("data-uk-switcher-item", "next");
                                console.log("Stay V");
                            }

                        }else{
                            $('#button-payment').removeAttr("data-uk-switcher-item", "next");
                            payment == false;
                        }
                       
                });

                var stripeResponseHandler = function(status, response) {
                    var form = $('#form-payment');
                    if (response.error) {
                        // Show the errors on the form
                        form.find('.payment-errors').text(response.error.message);
                        $('#button-payment').prop('disabled', false);
                    
                    }else{
                    
                        // token contains id, last4, and card type
                        var token = response.id;

                        //Ajax request to the file that does the processing
                        var request = $.ajax({
                            url: "functions/ajax-payment.php",
                            method: "POST",
                            data: { stripeToken : token },
                            dataType: "json"
                        });
                                 
                        request.done(function(data) {

                            if(data.result == true){
                                $('.loading-payment').addClass('uk-hidden');
                                $('#button-payment').removeClass('button-secondary-solid-disabled').prop('disabled', false).attr("data-uk-switcher-item", "next").unbind().click();

                                var card_type = "";

                                if(data.information_1 == 'Visa'){
                                    card_type = 'visa';
                                }else if(data.information_1 == 'MasterCard'){
                                    card_type = 'mastercard';
                                }else if(data.information_1 == 'American Express'){
                                    card_type = 'amex';
                                }else if(data.information_1 == 'Discover'){
                                    card_type = 'discover';
                                }

                                $('#wrapper-card-logos').addClass('is_'+card_type).html('<li class="card_'+card_type+'"></li>');
                                $('#wrapper-card-last4').html(data.information_2);
                                console.log('Payment Added');

                                onCheckoutOption('Payment', card_type.toUpperCase());
                                dataLayer.push({‘checkoutProcess’: '2'});

                            }

                                
                        });
                                 
                        request.fail(function(jqXHR, textStatus, errorThrown) {

                            console.log("Error: "+textStatus);
                            $('.loading-payment').addClass('uk-hidden');
                            $('#button-payment').removeClass('button-secondary-solid-disabled').prop('disabled', false);
                            $('#error-message-card').html('There was a problem accepting your card. Please try again shortly or use a different card.');

                        });

                    }
                };


        </script>




        <script type="text/javascript">
            //calculating and displaying prices considering both delivery cost and discounts
            
                $('#delivery_option').change(function(){

                    var display_product_price_final         = $("#display_product_price_final").html();
                    var display_delivery_cost               = $("#display_delivery_cost").html();
                    var display_total                       = $("#display_total").html();

                    var delivery_option                     = $("#delivery_option").val();

                    $("#display_delivery_cost").html(delivery_option);

                    display_total = parseFloat(display_product_price_final) + parseFloat(delivery_option);

                    $("#display_total, #mobile_display_total").html(display_total.toFixed(2));
                });
                
        </script>



        <script type="text/javascript">
                $('#form-summary').submit(function(){
                
                    var form = $('#form-summary');                        
                    event.preventDefault();

                    //form processing via ajax to make a transaction

                    $('.loading-summary').removeClass('uk-hidden');
                    $('#button-summary, #mobile-button-summary').addClass('button-tertiary-solid-disabled');

                    var delivery_option = $('#delivery_option').val();
                    var coupon_code     = $('#coupon_code').val();


                    //Ajax request to the file that does the processing
                    var request = $.ajax({
                        url: "functions/ajax-summary.php",
                        method: "POST",
                        data: { delivery_option : delivery_option, coupon_code : coupon_code },
                        dataType: "json"
                    });
                             
                    request.done(function(data) {

                        if(data.result == true){

                            $('.loading-summary').addClass('uk-hidden');
                            $('#button-summary, #mobile-button-summary').removeClass('button-tertiary-solid-disabled');
                            $('.transaction-response').html('Your transaction was successful. Please wait a moment while you are redirected.');

                            onCheckoutOption('Payment Confirmation', 'Payment Confirmation');
                            dataLayer.push({‘checkoutProcess’: '3'});

                            window.dataLayer = window.dataLayer || []
                            dataLayer.push({
                                  'ecommerce': {
                                    'purchase': {
                                      'products': [{                            // List of productFieldObjects.
                                        'name': "<?php echo ucwords($product[0]->product_name); ?>",     // Name or ID is required.
                                        'id': "<?php echo $product_id; ?>",
                                        'price': "<?php echo $product_price; ?>",
                                        'brand': "<?php echo ucwords($brand_name); ?>",
                                        'category': "<?php echo ucwords($product[0]->product_tags); ?>",
                                        'variant': "<?php echo ucwords($product_option_name); ?>",
                                        'quantity': "<?php echo $product_quantity; ?>",
                                        'coupon': data.coupon_code                           // Optional fields may be omitted or set to empty string.
                                       }]
                                    }
                                }
                            });



                            //go to the thank you page
                            //console.log('true');

                            window.location = 'thankyou.php';
                        }else if(data.error){

                            $('.summary').html(data.error);
                            $('.loading-summary').addClass('uk-hidden');

                        }else{
                            $('.transaction-response').html(data.result);
                            $('.loading-summary').addClass('uk-hidden');
                        }

                    });

                        

                        
                                     
                    request.fail(function(jqXHR, textStatus, errorThrown) {
                        console.log("Error: "+errorThrown);
                        $('.loading-summary').addClass('uk-hidden');
                        $('#button-summary, #mobile-button-summary').removeClass('button-tertiary-solid-disabled').prop('disabled', false);
                    });
                    
                });
        </script>



        <script type="text/javascript">
            $('#button-coupon-code').click(function(){

                event.preventDefault();
                var coupon_code = $('#coupon_code').val();
                var product_id  = <?php echo $product_id; ?>

                //Ajax request to the file that does the processing
                    var request = $.ajax({
                        url: "functions/ajax-coupon-code.php",
                        method: "POST",
                        data: { coupon_code : coupon_code, product_id : product_id },
                        dataType: "json"
                    });
                             
                    request.done(function(data) {

                        if(data.status == 'success'){

                            //User visible change to the UI when the coupon code is applied
                            $('#display_product_price').addClass("text-strike-through");
                            $('#display_product_price_discounted').html(data.value).css({"display": "inline-block", "color": "red", "font-weight" : "700"}).removeClass("uk-hidden");
                            $('#display_product_price_final').html(data.value);
                            $('.coupon-code-response').html('Coupon code applied.');

                            //Price change according as the coupon code is applied after the shipping has been selected
                            var delivery_option  = $("#delivery_option").val();
                            display_total = parseFloat(data.value) + parseFloat(delivery_option);
                            $("#display_total, #mobile_display_total").html(display_total);                       
                            
                            console.log('true');
                        }else{
                            $('.coupon-code-response').html(data.status);
                        }
                            
                    });
                             
                    request.fail(function(jqXHR, textStatus, errorThrown) {
                        console.log("Error: "+errorThrown);
                    });
                    
            })
        </script>

    </body>
</html>
