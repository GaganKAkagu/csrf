<?php


require_once('core/init.php');


function datetimetounix($date, $time){
	//d.m.Y h:i
	$fulldate = $date.' '.$time;
	return strtotime($fulldate);
}

//data fetched for displaying the timer
//2018/01/01 00:00:00
//year/month/day hour:min:sec
function unixtodatetime($unixtimestamp){
	return date ( 'Y/m/d H:i:s', $unixtimestamp);
}



function getremainingtime($start_date, $start_time, $end_date, $end_time, $drop_frequency){

	//Getting a unix timestamp from user readable dates - for start date
	$startunix = datetimetounix($start_date, $start_time);

	//Getting a unix timestamp from user readable dates - for end date
	$endunix   = datetimetounix($end_date, $end_time);

	//Calculating the differences in the timestamp and dividing them into the drop frequency
	$diffunix = $endunix-$startunix;

	$dropunix = round($diffunix/$drop_frequency);

	$array_time[0] = $startunix;

	for($k=0;$k<$drop_frequency;$k++){	
		$array_time[] = $array_time[$k]+$dropunix;
	}

	//Prints Remaining Time for a particular time block (When should the timer finish with formatted date)
	for($c=1;$c<=count($array_time);$c++){
		if(time() >= $array_time[$c-1] && time() <= $array_time[$c]){
			$frontend_dp_time = $array_time[$c];
		}else if(time() >= $array_time[count($array_time)-1]){
			$frontend_dp_time = end($array_time);
		}
	}

	return $dp_time = unixtodatetime($frontend_dp_time);

}



function getdropcount($start_date, $start_time, $end_date, $end_time, $drop_frequency){

	//Getting a unix timestamp from user readable dates - for start date
	$startunix = datetimetounix($start_date, $start_time);

	//Getting a unix timestamp from user readable dates - for end date
	$endunix   = datetimetounix($end_date, $end_time);

	//Calculating the differences in the timestamp and dividing them into the drop frequency
	$diffunix = $endunix-$startunix;

	$dropunix = round($diffunix/$drop_frequency);

	$array_time[0] = $startunix;

	for($k=0;$k<$drop_frequency;$k++){	
		$array_time[] = $array_time[$k]+$dropunix;
	}

	$array_length = count($array_time);

	//Prints Remaining Time for a particular time block (When should the timer finish with formatted date)
	//for($c=1;$c<=count($array_time);$c++){
		if(time() >= $array_time[$array_length-2] && time() <= $array_time[$array_length-1]){
			$drop_count = 'LAST';
		}else if(time() >= $array_time[$array_length-3] && time() <= $array_time[$array_length-2]){
			$drop_count = 1;
		}else if(time() >= $array_time[$array_length-4] && time() <= $array_time[$array_length-3]){
			$drop_count = 2;
		}else if(time() >= $array_time[$array_length-1]){
			$drop_count = 0;
		}
	//}

	return $drop_count;

}




function getprice($start_date, $start_time, $end_date, $end_time, $start_price, $end_price, $drop_frequency, $time = false){

		//Calculating the differences in the initial and final prices and dividing them into the drop frequency
		$diffprice = $start_price - $end_price;

		$dropprice = $diffprice/$drop_frequency;

		$array_price[0] = $start_price;

		for($p=0;$p<$drop_frequency;$p++){	
			$array_price[] = $array_price[$p]-$dropprice;
		}

		//Getting a unix timestamp from user readable dates - for start date
		//$startunix = datetimetounix($start_date, $start_time);
		$fulldate_start = $start_date.' '.$start_time;
		$startunix = strtotime($fulldate_start);

		//Getting a unix timestamp from user readable dates - for end date
		//$endunix   = datetimetounix($end_date, $end_time);
		$fulldate_end = $end_date.' '.$end_time;
		$endunix = strtotime($fulldate_end);

		//Calculating the differences in the timestamp and dividing them into the drop frequency
		$diffunix = $endunix-$startunix;

		$dropunix = $diffunix/$drop_frequency;

		$array_time[0] = $startunix;

		for($k=0;$k<$drop_frequency;$k++){	
			$array_time[] = $array_time[$k]+$dropunix;
		}

		if($time !== false){
			//Displaying Current Price
			if($time >= $startunix && $time <= $endunix){

				for($j=1;$j<=$drop_frequency;$j++){
					if($time >= $array_time[$j-1] && $time <= $array_time[$j]){
						$current_price = $array_price[$j-1]; 
					}else if($time >= end($array_time)){
						$current_price = end($array_price);
					}
				}
				//number_format($current_price ,2 , ".", ",");

				return number_format($current_price ,2 , ".", ",");

			}elseif($time >= $startunix && $time >= $endunix){
				//use the reserve price instead of calculating it.
				//echo 'Timer stops and reserve price is used.<br/>';
				$current_price = $end_price;
				
				return number_format($current_price ,2 , ".", ",");	
			}
		}else{
			//Displaying Current Price
			if(time() >= $startunix && time() <= $endunix){

				for($j=1;$j<=$drop_frequency;$j++){
					if(time() >= $array_time[$j-1] && time() <= $array_time[$j]){
						$current_price = $array_price[$j-1]; 
					}else if(time() >= end($array_time)){
						$current_price = end($array_price);
					}
				}
				//number_format($current_price ,2 , ".", ",");

				return number_format($current_price ,2 , ".", ",");

			}elseif(time() >= $startunix && time() >= $endunix){
				//use the reserve price instead of calculating it.
				//echo 'Timer stops and reserve price is used.<br/>';
				$current_price = $end_price;
				
				return number_format($current_price ,2 , ".", ",");	
			}
		}

		

}


if(Input::exists('get')){

	if(Input::get('product_id') && Input::get('action') == 'get_price'){

		$products = new Product();
		$product = $products->find('product_id', Input::get('product_id'));

		$price  	= getprice($product[0]->product_bid_start_date, $product[0]->product_bid_start_time, $product[0]->product_bid_end_date, $product[0]->product_bid_end_time, $product[0]->product_price_start, $product[0]->product_price_end, $product[0]->product_drop_frequency);

		echo json_encode(["price" => "$price", "data" => "parsed"]);
	}


	if(Input::get('product_id') && Input::get('action') == 'get_details'){

		$products = new Product();
		$product = $products->find('product_id', Input::get('product_id'));

		$time  		= getremainingtime($product[0]->product_bid_start_date, $product[0]->product_bid_start_time, $product[0]->product_bid_end_date, $product[0]->product_bid_end_time, $product[0]->product_drop_frequency);
		$price  	= getprice($product[0]->product_bid_start_date, $product[0]->product_bid_start_time, $product[0]->product_bid_end_date, $product[0]->product_bid_end_time, $product[0]->product_price_start, $product[0]->product_price_end, $product[0]->product_drop_frequency);
		$drop_count = getdropcount($product[0]->product_bid_start_date, $product[0]->product_bid_start_time, $product[0]->product_bid_end_date, $product[0]->product_bid_end_time, $product[0]->product_drop_frequency);

		$current_time = unixtodatetime(time());
		echo json_encode(["price" => "$price", "timer" => "$time", "current_time" => "$current_time", "drop_count" => "$drop_count"]);
	}
	
}



// to use the function, the page need to have the class $products = new Product(); already defined and used
function get_current_countdown($product_id){

	

}



?>