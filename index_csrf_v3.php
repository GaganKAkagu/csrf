<?php
	
	session_start();

	require_once 'models/Transfer_money.php';    

	
	if(isset($_POST['amount'])){
		if(!empty($_POST['amount'])){
			if(Transfer_money::check($_POST['token'])){
				$token_age = time() - $_SESSION['token_time'];
				if($token_age <= 300){
					echo 'Safe Form Submit';
				}else{
					echo 'Five minutes has passed.';
				}				
			}else{
				echo 'Token does not match';
			}
		}else{
		echo 'Please enter the amount';
	}
	}else{
		echo 'Please enter the amount';
	}

?>

<!DOCTYPE html>
<html>
<head>
  	
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <title>Form Token Test Example</title>
<body>

<form action="" method="post">
  <input type="number" min="1" name="amount" placeholder="Enter amount">
  <input type="submit" value="Transfer">
  <input type="hidden" name="token" value="<?php echo Transfer_money::generate(); ?>">
  
</form>



</body>
</html>

<?php
echo $_SESSION['token'];
echo "<br />\n";
echo $_SESSION['token_time'];

?>