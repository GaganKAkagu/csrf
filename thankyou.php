<?php 

    require('core/init.php'); 

    $users              = new User();
    $brands             = new Brand();
    $products           = new Product();
    $purchase_detail    = new Purchase_Detail();

    $images     = new Product_Image();

    /*if(!$users->isLoggedIn() && $_SESSION['product_id']){
        Redirect::to(ROOT_URL.'?status=user-login-required');
    }*/

    
    $image      = $images->find('product_image_product_id', 100);
    $purchase   = $purchase_detail->find_order(251);
    $product    = $products->find('product_id', $purchase[0]->purchase_product_id);

    //include_once('emails/purchase-confirmation.php');


?>

<!doctype html>


<html class="no-js" lang="" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Akagu - Thank You</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <meta property="fb:admins" content="918678374846604" />
        <meta property="og:title" content="Akagu - Designer Fashion Marketplace" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="http://www.akagu.com.au/beta/administrator/functions/uploadedimages/<?php echo $image[0]->product_image_product_name; ?>" />
        <meta property="og:url" content="http://www.akagu.com.au/beta/product.php?product=<?php echo $product[0]->product_name_slug; ?>" />
        <meta property="og:site_name" content="<?php echo $product[0]->product_name; ?>" />


        <?php
            //including common stylesheets and favicons
            include_once('include/includes_header.php');
        ?>

    </head>
    <body>

        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!--Transaction Tracking-->
        <script>
            window.dataLayer = window.dataLayer || []
            dataLayer.push({
               
               'transactionId': "<?php echo $purchase[0]->purchase_reference_number; ?>",
               'transactionAffiliation': "<?php echo ucwords($product[0]->product_name_slug); ?>",
               'transactionTotal': "<?php echo (int)$purchase[0]->purchase_subtotal; ?>",
               'transactionTax': "<?php echo $purchase[0]->purchase_tax; ?>",
               'transactionShipping': "<?php echo $purchase[0]->purchase_shipping; ?>",
               'transactionProducts': [{
                   'sku': "<?php echo $product[0]->product_id; ?>",
                   'name': "<?php echo ucwords($product[0]->product_name_slug); ?>",
                   'category': "<?php echo ucwords($product[0]->product_tags); ?>",
                   'price': "<?php echo (int)$purchase[0]->purchase_subtotal; ?>",
                   'quantity': "<?php echo $purchase[0]->purchase_quantity; ?>"
               }]

            });
        </script>


        <?php include ('views/templates/header.php'); ?>
        
        <main>
            <section class="background-light-secondary section-small">
                <div class="uk-grid">
                    <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-small-top uk-margin-small-bottom uk-text-left mobile-block">
                        <div class="uk-width-small-1-1 max-width uk-container-center uk-margin-small-top uk-margin-small-bottom uk-text-left mobile-block">
                            <a href="<?php echo ROOT_URL; ?>" class="link-dark">Home</a> / <a href="<?php echo ROOT_URL; ?>thankyou.php" class="link-dark">Thank You</a>
                    </div>
                    </div>                  
                </div>
            </section>


            <section class="section-small">
                <div class="uk-grid">

                    <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-small-top uk-margin-small-bottom mobile-block">

                        <div class="uk-grid section-large mobile-section-large"> 
                            <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-width-large-1-2 uk-container-center section-border">  
                                <div class="section-medium uk-text-center">
                                    <img src="assets/images/icon-thank-you.png" class="uk-margin-top">
                                    <h3 class="text-section-title">Congratulations!</h3>
                                    <p>Your purchase for <strong><?php echo ucwords($product[0]->product_name); ?></strong> has been secured.</p>
                                    <p>You will receive a confirmation of your purchase via email.</p>
                                </div>
                                <div class="uk-text-center">
                                    
                                    <a href="<?php echo ROOT_URL.'invoice.php?payment_id='.$purchase[0]->purchase_reference_number; ?>" class="uk-button button-dark-outline uk-button-large uk-hidden-small">View Purchase</a>

                                    <a href="<?php echo ROOT_URL.'events.php'; ?>" class="uk-button button-primary-solid uk-button-large">Continue Shopping</a>
                                </div>
                                <hr>
                                <div class="uk-text-center">
                                    <div>Help us spread the word by telling your friends</div>
                                    <div class="section-medium">

                                        <a href="https://www.facebook.com/dialog/share?app_id=145634995501895&display=popup&href=https%3A%2F%2Fakagu.com.au%2Fbeta%2Fproduct.php?product=<?php echo $product[0]->product_name_slug; ?>" target="_blank"><div class="icon-facebook uk-icon-large button-facebook"></div></a>
                                        <a href="https://twitter.com/intent/tweet?text=https%3A%2F%2Fakagu.com.au%2Fbeta%2Fproduct.php?product=<?php echo $product[0]->product_name_slug; ?>" target="_blank"><div class="icon-twitter uk-icon-large button-twitter"></div></a>
                                        <!--<a href=""><div class="icon-instagram button-instagram" style="font-size: 24px; margin-top: 2px;display: inline-block;vertical-align: -2px;"></div></a>-->
                                        <a href="https://www.pinterest.com/pin/create/button/?url=http://www.akagu.com.au/beta/product.php?product=<?php echo $product[0]->product_name_slug; ?>&media=http://www.akagu.com.au/beta/administrator/functions/uploadedimages/<?php echo $image[0]->product_image_product_name; ?>&description=<?php echo $product[0]->product_name; ?>" target="_blank"><div class="icon-pinterest uk-icon-large button-pinterest"></div></a>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                            
                    </div>

                </div>                  

            </section>
            
           
            


            <?php include('views/templates/footer.php'); ?>




        </main>

        <script>window.jQuery || document.write('<script src="<?php echo ROOT_STATIC; ?>js/vendor/jquery-1.12.3.min.js"><\/script>')</script>
        <script src="<?php echo ROOT_STATIC; ?>js/uikit.js"></script>
        <script src="<?php echo ROOT_STATIC; ?>js/main.js"></script>
        <script src="<?php echo ROOT_STATIC; ?>js/search.js"></script>
        
        <script src="<?php echo ROOT_STATIC; ?>js/plugins.js"></script>

    </body>
</html>
