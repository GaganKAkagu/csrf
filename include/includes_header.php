
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<!-- Place favicon.ico in the root directory -->

<!--build:css assets/dist/css/child.min.css-->
<link rel="stylesheet" href="assets/css/uikit.css">
<link rel="stylesheet" href="assets/css/child.css">
<!--endbuild-->

<script src="<?php echo ROOT_STATIC; ?>js/vendor/modernizr-2.8.3.min.js"></script>


<link rel="icon" type="image/png" href="<?php echo ROOT_STATIC; ?>images/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="<?php echo ROOT_STATIC; ?>images/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo ROOT_STATIC; ?>images/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/x-icon" href="images/favicon.ico">

