

            $("#form-delivery").validate({
                errorElement: 'div',
                rules: {
                    address: {
                        required: true
                    },
                    suburb: {
                        required: true
                    },
                    state: {
                        required: true
                    },
                    postal_code: {
                        required: true
                    }
                },
                messages: {
                    address: {
                        required: "Address is required"
                    },
                    suburb: {
                        required: "Suburb is required"
                    },
                    state: {
                        required: "State is required"
                    },
                    postal_code: {
                        required: "Postal Code is requried"
                    }
                }
            });