        $(document).ready(function() {
            $("#form-payment").validate({
                errorElement: 'div',
                rules: {
                    credit_card_number: {
                        required: true,
                        creditcard: true
                    },
                    credit_card_cvv2: {
                        required: true,
                        digits: true,
                        maxlength: 4
                    },
                    expiry_month: {
                        required: true,
                        digits: true 
                    },
                    expiry_year: {
                        required: true,
                        digits: true
                    }
                },
                messages: {
                    credit_card_number: {
                        required: "Credit card number is required",
                        creditcard: "A valid credit card number is required"
                    },
                    credit_card_cvv2: {
                        required: "CCV is required",
                        digits: "Only enter digits",
                        maxlength: "Enter no more than 3 digits for Visa and Master card while 4 for American Express"
                    },
                    expiry_month: {
                        required: "Select Expiry Month",
                        digits: "Please only enter digits."
                    },
                    expiry_year: {
                        required: "Select Expiry Year",
                        digits: "Please only enter digits."
                    }
                }


            });
        });