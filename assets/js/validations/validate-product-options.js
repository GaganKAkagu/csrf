    //Products Page

    $('#product_size, #product_quantity').change(function(){
        select_check();
        select_validate();
        //console.log('Function triggered.');
    })


    $('.button-bid, .button-buy').click(function(){
        if($('#product_size :selected').val() == 0 || $('#product_quantity :selected').val() == 0){
        //if($(this).hasClass('button-primary-solid-disabled') && $('.button-bid-small').hasClass('mobile-button-primary-solid-disabled')){
            event.preventDefault();
            //console.log('Has class');
            $('#product_size, #product_quantity').focus().css("border", "1px solid red");
        }else{
            console.log('Submit Form');
        }
    })
  
 
    function select_check(){
        if($('#product_size :selected').val() != 0 && $('#product_quantity :selected').val() != 0){
            $('.button-bid-small').removeClass('mobile-button-primary-solid-disabled');
            $('.button-buy-small').removeClass('mobile-button-secondary-solid-disabled');
            
            $('.button-bid').removeClass('button-primary-solid-disabled');
            $('.button-buy').removeClass('button-secondary-solid-disabled');
        }else{
            $('.button-bid-small').addClass('mobile-button-primary-solid-disabled');
            $('.button-buy-small').addClass('mobile-button-secondary-solid-disabled');

            $('.button-bid').addClass('button-primary-solid-disabled');
            $('.button-buy').addClass('button-secondary-solid-disabled');
        }
    }


    function select_validate(){
        if($('#product_size').val() != 0){    
            $('#product_size').css("border", "1px solid #dddddd");
        }

        if($('#product_quantity').val() != 0){
            $('#product_quantity').css("border", "1px solid #dddddd");
        }
    }