<?php

// Database variables
$servername = "aa1usev0ag4x0kl.cyr0p8tmvp7b.ap-southeast-2.rds.amazonaws.com"; //database location
$user = "akaguc51agu"; //database username
$pass = "umDa8UhCKW"; //database password
$db_name = "akagu_test"; //database name



// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
// Set this to 0 once you go live or don't require logging.
define("DEBUG", 1);

// Set to 0 once you're ready to go live
define("USE_SANDBOX", 1);


define("LOG_FILE", "./ipn.log");



// Open Database Connection
$conn = new PDO("mysql:host=$servername;dbname=$db_name", $user, $pass);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


// Read POST data
// reading posted data directly from $_POST causes serialization
// issues with array data in POST. Reading raw POST data from input stream instead.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);

// Select data from the database
//$stmt = $conn->prepare("SELECT * FROM products WHERE product_id = 87"); 
//$stmt->execute();
//$result = $stmt->fetch(PDO::FETCH_OBJ);

// Declare static varibales
//$product_id = 19;
$action = 'buy';
//$product_quantity = 1;
//$delivery_option = 7;
//$time = 1473654820;     

//calculating the product price with the quantity accounted for
//$product_price = $result->product_rrp;
//$product_name = $result->product_name;
//$product_price  = $product_price * $product_quantity;

// static data
//$product_price = 10;
//$product_name = "curl_listener";
//$product_price  = $product_price * $product_quantity;


$myPost = array();
foreach ($raw_post_array as $keyval) {
	$keyval = explode ('=', $keyval);
	if (count($keyval) == 2)
		$myPost[$keyval[0]] = urldecode($keyval[1]);
}



           

       /*if($myPost){
           $fp = fopen('listener-result.json', 'w');
           fwrite($fp, json_encode($myPost));
           fclose($fp);
       }*/

//print_r($myPost);
//echo "The message from IPN was: <b>" .$myPost ."</b>";

// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
	$get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
	if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
		$value = urlencode(stripslashes($value));
	} else {
		$value = urlencode($value);
	}
	$req .= "&$key=$value";
}

echo "The message from IPN was: <b>" .$req ."</b>";

// Post IPN data back to PayPal to validate the IPN data is genuine
// Without this step anyone can fake IPN data

if(USE_SANDBOX == true) {
	// Declare empty string
	//$querystring = '';

	// Firstly Append paypal account to querystring
	//$querystring .= "?business=".urlencode($paypal_email)."&";

	// Append $product_name, $product_price, $product_id
	//$querystring .= "item_name=".urlencode($product_name)."&";
	//$querystring .= "amount=".urlencode($product_price)."&";
	//$querystring .= "item_number=".urlencode($product_id)."&";

	// Append paypal return addresses
	//$querystring .= "return=".urlencode(stripslashes($return_url))."&";
	//$querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
	//$querystring .= "notify_url=".urlencode($notify_url);

	$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr".$querystring;
} else {
	$paypal_url = "https://www.paypal.com/cgi-bin/webscr".$querystring;
}

$ch = curl_init($paypal_url);
if ($ch == FALSE) {
	return FALSE;
}

curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);



// CONFIG: Optional proxy configuration
//curl_setopt($ch, CURLOPT_PROXY, $proxy);
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);

// Set TCP timeout to 30 seconds
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
// of the certificate as shown below. Ensure the file is readable by the webserver.
// This is mandatory for some environments.

//$cert = __DIR__ . "./cacert.pem";
//curl_setopt($ch, CURLOPT_CAINFO, $cert);

$res = curl_exec($ch);
if (curl_errno($ch) != 0) // cURL error
	{
	if(DEBUG == true) {	
		error_log(print_r(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE));
	}
	curl_close($ch);
	exit;

} else {
		// Log the entire HTTP response if debug is switched on.
		if(DEBUG == true) {
			error_log(print_r(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE));
			error_log(print_r(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE));
		}
		curl_close($ch);
}

// Inspect IPN validation result and act accordingly

// Split response headers and payload, a better way for strcmp
$tokens = explode("\r\n\r\n", trim($res));
$res = trim(end($tokens));

if (strcmp ($res, "VERIFIED") == 0) {
	// check whether the payment_status is Completed
	// check that txn_id has not been previously processed
	// check that receiver_email is your PayPal email
	// check that payment_amount/payment_currency are correct
	// process payment and mark item as paid.

	// assign posted variables to local variables
	$item_name = $_POST['item_name'];
	$item_number = $_POST['item_number'];
	$payment_status = $_POST['payment_status'];
	$payment_amount = $_POST['mc_gross'];
	$payment_currency = $_POST['mc_currency'];
	$txn_id = $_POST['txn_id'];
	$receiver_email = $_POST['receiver_email'];
	$payer_email = $_POST['payer_email'];
	$quantity = $_POST['quantity'];
	$paypal_payment_fee = $_POST['mc_fee'];
	$shipping = $_POST['shipping'];
	$payment_date = $_POST['payment_date'];	
	//$gst = $_POST['tax'];

	$arr = array();
	//$delivery_option = 7;	

    //basic math calculation - final product price
    $item_price = $payment_amount - $shipping;
    $gst_static  = (1/11) * $item_price;
    $net_price  = $item_price - $gst_static;
    $total_price  = $net_price + $gst_static + $shipping;

    array_push($arr, array(

               "purchase_product_id" => $item_number, 
               "purchase_quantity" => $quantity,
               "purchase_reference_number" => $txn_id,
               "purchase_status" => $payment_status,
               "purchase_currency" => $payment_currency,
				"total_amount" => $total_price,
				"purchase_description" => $item_name,
				"gst(tax)" => $gst_static,
				"net_price" => $net_price,
				"shipping" => $delivery_option,
				"paypal_fee" => $paypal_payment_fee,
				"payment_method" => "PayPal Payment",
				"shipping" => $shipping,
				"payment_date" => $payment_date

           ));
           

       if($arr){
           $fp = fopen('listener-result.json', 'w');
           fwrite($fp, json_encode($arr));
           fclose($fp);
       }

       //save purchase details
	    try{
	    		// exclude product_id as it's an auto-incremental field
	    		// use _Session_userID for purchase_user_id on production site

	            $statement = $conn->prepare("INSERT INTO purchase_details(purchase_user_id, purchase_product_id,purchase_quantity, purchase_reference_number,
                                purchase_status, purchase_currency, purchase_subtotal,
                                purchase_shipping, purchase_tax, purchase_description,
                                purchase_invoice_number, purchase_timestamp)
            VALUES(:purchase_user_id, :purchase_product_id,:purchase_quantity, :purchase_reference_number,
                                :purchase_status, :purchase_currency, :purchase_subtotal,
                                :purchase_shipping, :purchase_tax, :purchase_description,
                                :purchase_reference_number, :purchase_timestamp)");

        $statement->execute(array(

                "purchase_user_id" => 27,
                "purchase_product_id" => $item_number, 
                "purchase_quantity" => $quantity,
                "purchase_reference_number" => $txn_id,
                "purchase_status" => $payment_status,
                "purchase_currency" => $payment_currency,
                "purchase_subtotal" => $payment_amount,
                "purchase_description" => $item_name,
                "purchase_tax" => $gst_static,
                "purchase_shipping" => $shipping,
                "purchase_timestamp" => $payment_date
        ));
	                
	            

	    } catch (Exception $e){
	            die($e->getMessage());
	        }

    
	
	
	if(DEBUG == true) {
		error_log(print_r(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE));
	}
} else if (strcmp ($res, "INVALID") == 0) {

// IPN invalid, log for manual investigation
    echo "The response from IPN was: <b>" .$res ."</b>";

	// log for manual investigation
	// Add business logic here which deals with invalid IPN messages
	if(DEBUG == true) {
		error_log(print_r(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE));
	}
}

?>