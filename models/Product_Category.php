<?php

class Product_Category{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}


	public function update($table, $fields = array(), $id = null){
		
		if(!$this->_db->update($table, $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('product_categories', $fields)){	 //check this line again
			throw new Exception('There was a problem adding the item.');
		}
	}

	public function delete($product_category_id){
		$field = 'product_category_id';
		//$delete = ;
		if(!$this->_db->delete('product_categories', array('product_category_id', '=', $product_category_id))){	 //check this line again
			throw new Exception('There was a problem deleting this product.');
		}
	}

	public function get($where = array()){
		return $this->_db->get('SELECT *', 'product_categories', $where);
	}


	public function find($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('product_categories', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}

	public function findall($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('product_categories', array($field, '>', $fields));
			return $data->results();
		}
		return false;
	}



	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>