<?php

class Search{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}

	public function lastinsertid(){
		return $this->_db->lastInsertId();
	}


	public function search($term){

		$this->_db->query_multiple("SELECT `products`.`product_name`, `products`.`product_name_slug`, `products`.`product_brand_id`, `products`.`product_tags`, `products`.`product_publish_status`, `brands`.`brand_name`, `brands`.`brand_id`, `brands`.`brand_publish_status` FROM products INNER JOIN brands ON `products`.`product_brand_id` = `brands`.`brand_id` where `products`.`product_publish_status` = 1 AND `brands`.`brand_publish_status` = 1 AND `products`.`product_name` LIKE \"%$term%\" OR `brands`.`brand_name` LIKE \"%$term%\" OR `products`.`product_tags` LIKE \"%$term%\" ORDER BY `products`.`product_name` ASC LIMIT 6");
		$rows = $this->_db->resultSet();
		return $rows;
	
	}

	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>