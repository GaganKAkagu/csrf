<?php

class Brand_Delivery{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}


	public function update($table, $fields = array(), $id = null){
		
		if(!$this->_db->update($table, $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('brand_deliveries', $fields)){	 //check this line again
			throw new Exception('There was a problem adding the item.');
		}
	}


	public function find($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('brand_deliveries', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}


	public function find_order($brand_id){

		$this->_db->query_multiple("SELECT * FROM brand_deliveries where `brand_delivery_brand_id` = '$brand_id' ORDER BY `brand_delivery_option_value` ASC");
		$rows = $this->_db->resultSet();
		return $rows;
	
	}


	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>