<?php

class Mailchimp_Subscriber{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}


	public function update($table, $fields = array(), $id = null){
		
		if(!$this->_db->update($table, $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('mailchimp_subscribers', $fields)){	 //check this line again
			throw new Exception('There was a problem adding the item.');
		}
	}

	public function delete($item_id){
		$field = 'subscriber_id';
		//$delete = ;
		if(!$this->_db->delete('mailchimp_subscribers', array('subscriber_id', '=', $item_id))){	 //check this line again
			throw new Exception('There was a problem deleting this item.');
		}
	}

	public function get($where = array()){
		return $this->_db->get('SELECT *', 'mailchimp_subscribers', $where);
	}


	public function find($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('mailchimp_subscribers', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}

	public function findall($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('mailchimp_subscribers', array($field, '>', $fields));
			return $data->results();
		}
		return false;
	}


	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>