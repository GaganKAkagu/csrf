<?php

class User{
	private $_db,
			$_data,
			$_sessionName,
			$_cookieName,
			$_isLoggedIn;


	public function __construct($user = null){
		$this->_db = DB::getInstance();

		$this->_sessionName = Config::get('session/session_name');
		$this->_cookieName  = Config::get('remember/cookie_name');

		if(!$user){
			if(Session::exists($this->_sessionName)){
				$user = Session::get($this->_sessionName);

				if($this->find($user)){
					$this->_isLoggedIn = true;
				}else{
					//process logout
				}

			}
		}else{
			$this->find($user);
		}

	}


	public function update($fields = array(), $id = null){
		
		if(!$id && $this->_isLoggedIn){
			$id = $this->data()->user_id;
		}

		if(!$this->_db->update('users', $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('users', $fields)){	 //check this line again
			throw new Exception('There was a problem creating an account.');
		}
	}


	public function find($email = null){
		if($email){
			$field = (is_numeric($email)) ? 'user_id': 'email';
			$data = $this->_db->get('users', array($field, '=', $email));

			if($data->count()){
				$this->_data = $data->first();
				return true;
			}
		}
		return false;
	}


	public function findemail($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('users', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}
	

	public function login($email = null, $password = null, $remember = false){
		
		
		if(!$email && !$password && $this->exists()){

			Session::put($this->_sessionName, $this->data()->user_id);

		}else{

	
			$user = $this->find($email);

				if($user){

					$findemail = $this->findemail('email', $email);

					if($findemail[0]->status == '1'){

						if($this->data()->password === Hash::make($password, $this->data()->salt)){

							Session::put($this->_sessionName, $this->data()->user_id);
							return 'true';
							
						}else{
							return 'The password you entered was incorrect.';
						}

					}else{

						return 'Please check your email to confirm your registration on Akagu.';
					}


				}else{
					return 'Your email was not found in the system.';
				}
		}
	}


	public function hasPermission($key){
		$group = $this->_db->get('groups', array('group_id', '=', $this->data()->group));
		
		if($group->count()){
			$permissions = json_decode($group->first()->permission, true);

			if($permissions[$key] == true){
				return true;
			}

		}
		return false;

	}

	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}

	public function logout(){
		$this->_db->delete('users_session', array('user_id',  '=', $this->data()->user_id));
		Session::delete($this->_sessionName);
	}

	public function data(){
		return $this->_data;
	}

	public function isLoggedIn(){
		return $this->_isLoggedIn;
	}

}

?>