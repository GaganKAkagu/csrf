<?php

class Transfer_money{
	public static function generate(){
		$token = md5(uniqid(rand(), TRUE));
		$_SESSION['token_time'] = time();
		return $_SESSION['token'] = $token;
	}

	public static function check($token){
		if(isset($_SESSION['token']) && $token === $_SESSION['token']){
			unset($_SESSION['token']);
			return true;
		}
		return false;
	}
}

?>