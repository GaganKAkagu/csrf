<?php

class Setting{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}

	public function lastinsertid(){
		return $this->_db->lastInsertId();
	}

	public function update($table, $fields = array(), $id = null){
		
		if(!$this->_db->update($table, $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('settings', $fields)){	 //check this line again
			throw new Exception('There was a problem adding the item.');
		}
	}


	public function find($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('settings', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}

	public function findall($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('settings', array($field, '>', $fields));
			return $data->results();
		}
		return false;
	}

	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>