<?php

class User_Payment{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}


	public function update($table, $fields = array(), $id = null){
		
		if(!$this->_db->update($table, $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('user_payments', $fields)){	 //check this line again
			throw new Exception('There was a problem adding the item.');
		}
	}

	public function delete($user_id){
		//$delete = ;
		if(!$this->_db->delete('user_payments', array('user_payment_user_id', '=', $user_id))){	 //check this line again
			throw new Exception('There was a problem deleting this product.');
		}
	}

	public function get($where = array()){
		return $this->_db->get('SELECT *', 'user_payments', $where);
	}


	public function find($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('user_payments', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}


	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>