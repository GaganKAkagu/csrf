<?php

class Product_Image{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}


	public function update($table, $fields = array(), $id = null){
		
		if(!$this->_db->update($table, $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('product_images', $fields)){	 //check this line again
			throw new Exception('There was a problem adding the item.');
		}
	}


	public function delete($image_id){
		if(!$this->_db->delete('product_images', array('product_image_product_id', '=', $image_id))){	 //check this line again
			throw new Exception('There was a problem deleting this item.');
		}
	}


	public function get($where = array()){
		return $this->_db->get('SELECT *', 'product_images', $where);
	}


	public function find($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('product_images', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}


	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>