<?php

class Product_Option{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}

	public function lastinsertid(){
		return $this->_db->lastInsertId();
	}

	public function update($table, $fields = array(), $id = null){
		
		if(!$this->_db->update($table, $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('product_options', $fields)){	 //check this line again
			throw new Exception('There was a problem adding the item.');
		}
	}

	public function delete($product_id){
		$field = 'product_options_product_id';

		if(!$this->_db->delete('product_options', array('product_options_product_id', '=', $product_id))){	 //check this line again
			throw new Exception('There was a problem deleting this product.');
		}
	}

	public function get($where = array()){
		return $this->_db->get('SELECT *', 'product_options', $where);
	}


	public function find($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('product_options', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}

	public function findall($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('product_options', array($field, '>', $fields));
			return $data->results();
		}
		return false;
	}


	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>