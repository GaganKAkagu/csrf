<?php

class Coupon{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}

	public function lastinsertid(){
		return $this->_db->lastInsertId();
	}

	public function update($table, $fields = array(), $id = null){
		
		if(!$this->_db->update($table, $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('coupons', $fields)){	 //check this line again
			throw new Exception('There was a problem adding the item.');
		}
	}

	public function delete($coupon_id){
		$field = 'coupon_id';
		//$delete = ;
		if(!$this->_db->delete('coupons', array('coupon_id', '=', $coupon_id))){	 //check this line again
			throw new Exception('There was a problem deleting this coupon.');
		}
	}

	public function get($where = array()){
		return $this->_db->get('SELECT *', 'coupons', $where);
	}


	public function find($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('coupons', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}
	

	public function findall($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('coupons', array($field, '>', $fields));
			return $data->results();
		}
		return false;
	}


	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>