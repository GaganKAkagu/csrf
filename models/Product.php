<?php

class Product{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}

	public function lastinsertid(){
		return $this->_db->lastInsertId();
	}


	public function events($filter){

		$this->_db->query_multiple("SELECT * FROM products where `product_publish_status` = 1 ". (empty($filter) ? "" : "AND `product_tags` = '$filter'"));
		$rows = $this->_db->resultSet();
		return $rows;
	
	}

	public function event_type($type){

		if($type == 'event'){
			$type = 1;
		}else if($type == 'buy'){
			$type = 0;
		}

		$this->_db->query_multiple("SELECT * FROM products where `product_publish_status` = 1 AND `product_sale_type` = '".$type."' ");
		$rows = $this->_db->resultSet();
		return $rows;
	
	}


	public function events_filters($event_type, $filter_brands, $filter_categories, $page){

		if($event_type == 'upcoming' || $event_type == 'current' || $event_type == 'past'){
			$product_sale_type = 1;
		}else if($event_type == 'buy'){
			$product_sale_type = 0;
		}

        if($filter_brands){
            foreach($filter_brands as $brands){
                $statement_brands .= "`product_brand_id` = '".$brands."' OR ";
            }
        }

        $statement_brands = substr($statement_brands, 0, -4);


        $statement_categories = " AND ";

        if($filter_categories){
            foreach($filter_categories as $categories){
                $statement_categories .= "`product_tags` = '".$categories."' OR ";
            }
        }

        $statement_categories = substr($statement_categories, 0, -4);

        if($filter_brands && $filter_categories){
            
           $statement = $statement_brands .' '. $statement_categories;
        
        }else if($filter_categories){
            
            $statement = $statement_categories;

        }else if($filter_brands){

            $statement = $statement_brands;
        
        }else{
            $statement = '';
        }

        $limit  = 24;

	    if($page > 0){   
		    $offset     = ($page - 1) * $limit;
		    $statement .= ' LIMIT '.$limit.' OFFSET '.$offset;
		}else{
			$statement .= ' LIMIT '.$limit;
		}

		$this->_db->query_multiple("SELECT * FROM products where `product_publish_status` = 1 AND `product_sale_type` = ".$product_sale_type." $statement");

		$rows = $this->_db->resultSet();

		return $rows;
	
	}

	public function events_filter_counts($event_type, $filter_brands, $filter_categories){

		if($event_type == 'upcoming' || $event_type == 'current' || $event_type == 'past'){
			$product_sale_type = 1;
		}else if($event_type == 'buy'){
			$product_sale_type = 0;
		}

        $statement_brands = " AND ";

        if($filter_brands){
            foreach($filter_brands as $brands){
                $statement_brands .= "`product_brand_id` = '".$brands."' OR ";
            }
        }

        $statement_brands = substr($statement_brands, 0, -4);


        $statement_categories = " AND ";

        if($filter_categories){
            foreach($filter_categories as $categories){
                $statement_categories .= "`product_tags` = '".$categories."' OR ";
            }
        }

        $statement_categories = substr($statement_categories, 0, -4);

        if($filter_brands && $filter_categories){
            
           $statement = $statement_brands .' '. $statement_categories;
        
        }else if($filter_categories){
            
            $statement = $statement_categories;

        }else if($filter_brands){

            $statement = $statement_brands;
        
        }else{
            $statement = '';
        }


		$this->_db->query_multiple("SELECT count(product_id) count FROM products where `product_publish_status` = 1 AND `product_sale_type` = ".$product_sale_type." $statement");

		$rows = $this->_db->resultSet();

		return $rows;
	
	}

	public function events_count($event_type){

		$statement = ' AND ';
		$time = time();

		if($event_type == 'upcoming'){

			$statement .= "`product_bid_start_timestamp` > '".$time."'";

		}else if($event_type == 'current'){

			$statement .= "`product_bid_start_timestamp` < '".$time."' AND `product_bid_end_timestamp` > '".$time."'";

		}else if($event_type == 'past'){

			$statement .= "`product_bid_end_timestamp` < '".$time."'";

		}else{
			$statement .= '';
		}


		$this->_db->query_multiple("SELECT count(product_id) count FROM products where `product_publish_status` = 1 $statement");

		$rows = $this->_db->resultSet();

		return $rows;
	
	}


	public function events_brand($brand_id, $filter){

		$this->_db->query_multiple("SELECT * FROM products where `product_publish_status` = 1 AND `product_brand_id` = '$brand_id'". (empty($filter) ? "" : "AND `product_tags` = '$filter'"));
		$rows = $this->_db->resultSet();
		return $rows;
	
	}


	public function events_index($filter){

		$this->_db->query_multiple("SELECT * FROM products where `product_publish_status` = 1 AND `product_sale_type` = 1 ORDER BY RAND() LIMIT 8");
		$rows = $this->_db->resultSet();
		return $rows;
	
	}

	public function events_index_buy(){

		$this->_db->query_multiple("SELECT * FROM products where `product_publish_status` = 1 AND `product_sale_type` = 0 ORDER BY RAND() LIMIT 8");
		$rows = $this->_db->resultSet();
		return $rows;
	
	}


	public function events_image_resize($brand_id){

		$this->_db->query_multiple("SELECT `product_images`.`product_image_id`, `product_images`.`product_image_product_id`, `product_images`.`product_image_product_name`, `products`.`product_id`, `products`.`product_brand_id`, `brands`.`brand_id`
				FROM product_images
				INNER JOIN products
				ON product_images.product_image_product_id = products.product_id
				INNER JOIN brands
				ON products.product_brand_id = brands.brand_id WHERE products.product_brand_id = '$brand_id';");

		$rows = $this->_db->resultSet();
		return $rows;
	
	}


	public function list_product_tags($brand_id){

		$this->_db->query_multiple("SELECT DISTINCT `product_tags` FROM products where `product_publish_status` = 1 AND `product_tags` != '' ". (empty($brand_id) ? "" : "AND `product_brand_id` = '$brand_id'")." ORDER BY `product_tags` ASC");
		$rows = $this->_db->resultSet();
		return $rows;
	
	}


	public function update($table, $fields = array(), $id = null){
		
		if(!$this->_db->update($table, $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('products', $fields)){	 //check this line again
			throw new Exception('There was a problem adding the item.');
		}
	}

	public function delete($product_id){
		$field = 'product_id';
		//$delete = ;
		if(!$this->_db->delete('products', array('product_id', '=', $product_id))){	 //check this line again
			throw new Exception('There was a problem deleting this product.');
		}
	}

	public function get($where = array()){
		return $this->_db->get('SELECT *', 'products', $where);
	}


	public function find($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('products', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}
	

	public function findall($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('products', array($field, '>', $fields));
			return $data->results();
		}
		return false;
	}


	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>