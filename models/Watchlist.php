<?php

class Watchlist{
	private $_db,
			$_data,
			$_sessionName;

	public function __construct(){
		$this->_db = DB::getInstance();
	}

	public function lastinsertid(){
		return $this->_db->lastInsertId();
	}


	public function watchlist_check($user_id, $product_id){

		$this->_db->query_multiple('SELECT * FROM watchlists where `watchlist_user_id` = '.$user_id.' AND `watchlist_product_id` = '.$product_id);
		$rows = $this->_db->resultSet();
		return $rows;
	
	}

	public function update($table, $fields = array(), $id = null){
		
		if(!$this->_db->update($table, $id, $fields)){
			throw new Exception ('There was a problem updating.');
		}
	}


	public function create($fields = array()){
		if($this->_db->insert('watchlists', $fields)){	 //check this line again
			throw new Exception('There was a problem adding the item.');
		}
	}

	public function delete($product_id){

		if(!$this->_db->delete('watchlist_id', array('watchlist_id', '=', $product_id))){	 //check this line again
			throw new Exception('There was a problem deleting this product.');
		}
	}

	public function get($where = array()){
		return $this->_db->get('SELECT *', 'watchlists', $where);
	}


	public function find($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('watchlists', array($field, '=', $fields));
			return $data->results();
		}
		return false;
	}
	

	public function findall($field , $fields = array()){
		if($fields){
			$data = $this->_db->get('watchlists', array($field, '>', $fields));
			return $data->results();
		}
		return false;
	}


	public function exists(){
		return (!empty($this->_data)) ? true: false;
	}


	public function data(){
		return $this->_data;
	}

}

?>