<section class="background-dark-primary section-xlarge mobile-section-large ">
                <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-small-top uk-margin-small-bottom mobile-block">

                        <div class="uk-grid"> 
                            
                            <div class="uk-width-small-1-2 uk-width-large-1-5 uk-width-xlarge-1-5">
                                <div class="text-column-title text-light uk-margin-bottom">Akagu</div>
                                <div><a href="events.php?event=current" class="link-light">Current Events</a></div>
                                <div><a href="events.php?event=upcoming" class="link-light">Upcoming Events</a></div>
                                <div><a href="events.php?event=past" class="link-light">Past Events</a></div>
                                <div><a href="shop-by-brands.php" class="link-light">Shop By Brand</a></div>
                            </div>

                            <div class="uk-width-small-1-2 uk-width-large-1-5 uk-width-xlarge-1-5">
                                <div class="text-column-title text-light uk-margin-bottom">About</div>
                                <div><a href="about.php" class="link-light">About Us</a></div>
                                <div><a href="http://akagu.com.au/blog/" target="_blank" class="link-light">Blog</a></div>
                                <div><a href="careers.php" class="link-light">Careers</a></div>
                                <div><a href="contact.php" class="link-light">Contact</a></div>
                            </div>

                            
                            <div class="uk-width-small-1-2 uk-width-large-1-5 uk-width-xlarge-1-5">
                            <div class="uk-visible-small">&nbsp;</div>
                                <div class="text-column-title text-light uk-margin-bottom">Services</div>
                                <div><a href="track-order.php" class="link-light">Track Order</a></div>
                                <div><a href="support.php" class="link-light">Support</a></div>
                                <div><a href="faq.php" class="link-light">FAQ</a></div>
                            </div>



                            <div class="uk-width-small-1-2 uk-width-large-1-5 uk-width-xlarge-1-5">
                            <div class="uk-visible-small">&nbsp;</div>
                                <div class="text-column-title text-light uk-margin-bottom">Legal</div>
                                <div><a href="privacy.php" class="link-light">Privacy</a></div>
                                <div><a href="terms-and-conditions.php" class="link-light">Terms & Conditions</a></div>
                                <div><a href="shipping-and-returns.php" class="link-light">Shipping & Returns</a></div>
                            </div>

                            <div class="uk-width-small-1-2 uk-width-large-1-5 uk-width-xlarge-1-5">
                            <div class="uk-visible-small">&nbsp;</div>
                                <div class="text-column-title text-light uk-margin-bottom">Supplier</div>
                                <div><a href="" class="link-light">Supplier Testimonials</a></div>
                                <div><a href="sell-through-us.php" class="link-light">Sell your products on Akagu</a></div>
                            </div>

                        </div>
                </div>

            </section>

            <section class="section-medium">
                <div class="uk-grid">
                    <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-small-top uk-margin-small-bottom mobile-block">   
                    
                        <div class="uk-float-left">
                             <a href="https://www.facebook.com/akaguofficial/" target="_blank"><span class="icon-facebook uk-icon-small button-facebook"></span></a>
                             <a href="https://twitter.com/akaguofficial" target="_blank"><span class="icon-twitter uk-icon-small button-twitter"></span></a>
                             <a href="https://www.instagram.com/akagu_official/" target="_blank"><span class="icon-instagram button-instagram" style="font-size: 17px; margin-left: 3px;"></span></a>
                        </div>

                        <div class="uk-text-center">
                            Copyright Akagu <?php echo date(Y); ?>
                        </div>

                    </div>
                </div>

            </section>