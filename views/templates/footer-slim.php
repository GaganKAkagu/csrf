<section class="background-dark-primary section-large">
                <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-small-top uk-margin-small-bottom">
                            
                            <div class="uk-width-small-1-1 uk-text-center">
                                <div class="uk-margin-bottom">
                                    <a href="terms-and-conditions.php" class="link-light">Terms and Conditions</a>
                                    <span class="text-light uk-margin-small-right">/ </span> 
                                    <a href="privacy.php" class="link-light"> Privacy Policy</a>
                                </div>
                                <div class="text-light">
                                    Copyright Akagu <?php echo date(Y); ?>
                                </div>
                            </div>
                </div>

            </section>