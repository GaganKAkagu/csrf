        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TTVWCR"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TTVWCR');
        <!-- End Google Tag Manager -->

        </script>

        <header class="background-dark-primary">
            <div class="uk-grid">
                <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-top uk-margin-bottom mobile-block mobile-margin-small-bottom">
                    <div class="logo uk-float-left uk-margin-small-top">
                        <a href="<?php echo ROOT_URL; ?>"><img src="<?php echo ROOT_STATIC; ?>images/logo-light.svg" alt="Akagu" width="110" class="logo uk-margin-right"></a>
                        <a href="events.php?event=<?php echo $event_type; ?>" class="link-light-primary uk-hidden-small">What's On</a>
                        <a href="shop-by-brands.php" class="link-light-primary uk-hidden-small">Brands</a>
                        <a href="index.php#how-it-works" class="link-light-primary uk-hidden-small">How Akagu Works?</a>
                        <a href="<?php echo ROOT_URL; ?>blog" class="link-light-primary uk-hidden-small">Blog</a>
                    </div>

                    <div class="uk-float-right uk-margin-small-top mobile-margin-small-top">
                        <a id="search-primary-button" href="#" class="link-light-primary-icon" data-uk-toggle="{target:'#search', animation:'uk-animation-fade'}"><i class="icon-search uk-icon-small flip-horizontal"></i></a>
                        <a href="watchlist.php" class="link-light-primary-icon"><i class="icon-heart-outline-light uk-icon-small"></i></a>
                    
                    <?php
                        if(!$users->isLoggedIn()){
                    ?>
                        <a href="<?php echo ROOT_URL_SECURE; ?>register.php?next=<?php echo $current_url; ?>" class="tracking-header-signup-button button-light-outline uk-margin-left link-muted mobile-margin-small-left">SIGN UP</a>
                    <?php
                        }else{
                            $firstname = explode(' ', $users->data()->fullname);
                            echo '<a href="'.ROOT_URL_SECURE.'account.php" class="link-muted"><span class="text-light uk-margin-left mobile-margin-small-left"><span class="uk-hidden-small">Hi, </span>'. $firstname[0].'</span></a><span class="text-light"> |  </a>
                                 <a href="logout.php" class="link-muted"><span class="text-light">Logout</span></a>';
                        }
                    ?>

                    </div>
                </div>                  
            </div>
        </header>


            <section id="search" class="search uk-hidden">
                <div class="uk-grid">
                    <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-small-top uk-margin-small-bottom uk-text-center max-width mobile-block">
                        <div class="section-medium uk-clearfix">
                            <div class="uk-grid">
                                <div class="uk-width-small-9-10">
                                    <form class="uk-form" method="post" action="">
                                    
                                        <input id="searchbox" type="text" class="searchbox uk-form-row uk-width-small-1-1 uk-margin-top-remove" placeholder="Search products like skirts or dresses or brands like Karen Walker or Jaz & Alex">
                                        
                                    </form>
                                        
                                </div>
                                <a href="javascript:void();" class="close close-search uk-float-right link-secondary uk-margin-top uk-margin-left" data-uk-toggle="{target:'#search', animation:'uk-animation-fade'}">
                                            
                                    <div id="mobile-nav" class="uk-align-center open">
                                        <span class="nav-top-bar"></span>
                                        <span class="nav-top-bar"></span>
                                        <span class="nav-top-bar"></span>
                                    </div>

                                 </a>
                             
                            </div>

                            <!--Search result shows up here-->
                            <div class="wrapper-search-results uk-margin-top uk-text-left">
                                <div class="no-results"></div>                         
                            </div>


                        </div>
                    </div>                  
                </div>
            </section>
            <section id="overlay-wrapper" class="overlay uk-hidden"></section>
