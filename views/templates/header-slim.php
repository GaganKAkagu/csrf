		<!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TTVWCR"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TTVWCR');
        <!-- End Google Tag Manager -->

        </script>
		
		<header class="background-dark-primary">
            <div class="uk-grid">
                <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-top uk-margin-bottom">
                    <div class="logo-slim uk-text-center uk-margin-small-top">
                        <a href="<?php echo ROOT_URL; ?>"><img src="<?php echo ROOT_STATIC; ?>images/logo-light.svg" alt="Akagu" width="110" class="logo"></a>
                    </div>
                </div>                  
            </div>
        </header>