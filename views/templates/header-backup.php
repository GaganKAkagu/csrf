        <header class="background-dark-primary">
            <div class="uk-grid">
                <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-top uk-margin-bottom mobile-block mobile-margin-small-bottom">
                    <div class="logo uk-float-left uk-margin-small-top">
                        <a href="<?php echo ROOT_URL; ?>"><img src="<?php echo ROOT_URL; ?>assets/images/logo-light.svg" alt="Akagu" width="110" class="logo uk-margin-right"></a>
                        <a href="events.php" class="link-light-primary uk-hidden-small">What's On</a>
                        <a id="brands-primary-button" href="#" class="link-light-primary uk-hidden-small" data-uk-toggle="{target:'#brands', animation:'uk-animation-fade'}">Brands</a>
                        <a href="index.php#how-it-works" class="link-light-primary uk-hidden-small">How Akagu Works?</a>
                    </div>

                    <div class="uk-float-right uk-margin-small-top mobile-margin-small-top">
                        <a id="search-primary-button" href="#" class="link-light-primary-icon" data-uk-toggle="{target:'#search', animation:'uk-animation-fade'}"><i class="icon-search uk-icon-small flip-horizontal"></i></a>
                        <a href="watchlist.php" class="link-light-primary-icon"><i class="icon-heart-outline-light uk-icon-small"></i></a>
                    
                    <?php
                        if(!$users->isLoggedIn()){
                    ?>
                        <a href="login.php?next=<?php echo $current_url; ?>" class="button-light-outline uk-margin-left link-muted mobile-margin-small-left">LOGIN</a>
                    <?php
                        }else{
                            $firstname = explode(' ', $users->data()->fullname);
                            echo '<a href="account.php" class="link-muted"><span class="text-light uk-margin-left mobile-margin-small-left"><span class="uk-hidden-small">Hi, </span>'. $firstname[0].'</span></a><span class="text-light"> |  </a>
                                 <a href="logout.php" class="link-muted"><span class="text-light">Logout</span></a>';
                        }
                    ?>

                    </div>
                </div>                  
            </div>
        </header>

            <section id="brands" class="brands uk-hidden">
                <div class="uk-grid">
                    <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-small-top uk-margin-small-bottom uk-text-center max-width">
                        <div class="section-medium uk-clearfix">
                            <span class="uk-float-left">Our recommeded brands</span>
                            <a href="javascript:void();" class="close uk-float-right link-secondary" data-uk-toggle="{target:'#brands', animation:'uk-animation-fade'}">

                                <div id="mobile-nav" class="uk-align-center open">
                                    <span class="nav-top-bar"></span>
                                    <span class="nav-top-bar"></span>
                                    <span class="nav-top-bar"></span>
                                </div>

                            </a>
                        </div>
                        <div id="brands-wrapper" class="uk-grid section-small">
                            
                        <?php
                            $brands_listing = $brands->find('brand_publish_status', 1);

                            foreach($brands_listing as $brand_listing){
                                echo '<div class="uk-width-small-1-2 uk-width-medium-1-5 uk-width-large-1-5 uk-margin-bottom uk-text-center-small mobile-remove-padding"><a href="brand.php?brand='.$brand_listing->brand_slug .'" class="image transition"><img src="administrator/functions/uploadedimages/'.$brand_listing->brand_logo.'" alt="'.ucwords($brand_listing->brand_logo).'" /></a></div>';
                            }


                        ?>

                        </div>
                    </div>                  
                </div>
            </section>

            <section id="search" class="search uk-hidden">
                <div class="uk-grid">
                    <div class="uk-width-small-1-1 uk-width-large-8-10 uk-width-xlarge-7-10 max-width uk-container-center uk-margin-small-top uk-margin-small-bottom uk-text-center max-width mobile-block">
                        <div class="section-medium uk-clearfix">
                            <div class="uk-grid">
                                <div class="uk-width-small-9-10">
                                    <form class="uk-form" method="post" action="">
                                    
                                        <input id="searchbox" type="text" class="searchbox uk-form-row uk-width-small-1-1 uk-margin-top-remove" placeholder="Search products like skirts or dresses or brands like Karen Walker or Jaz & Alex">
                                        
                                    </form>
                                        
                                </div>
                                <a href="javascript:void();" class="close close-search uk-float-right link-secondary uk-margin-top uk-margin-left" data-uk-toggle="{target:'#search', animation:'uk-animation-fade'}">
                                            
                                    <div id="mobile-nav" class="uk-align-center open">
                                        <span class="nav-top-bar"></span>
                                        <span class="nav-top-bar"></span>
                                        <span class="nav-top-bar"></span>
                                    </div>

                                 </a>
                             
                            </div>

                            <!--Search result shows up here-->
                            <div class="wrapper-search-results uk-margin-top uk-text-left">
                                <div class="no-results"></div>                         
                            </div>


                        </div>
                    </div>                  
                </div>
            </section>
            <section id="overlay-wrapper" class="overlay uk-hidden"></section>
