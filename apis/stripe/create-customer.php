<?php

//initialising db connection and dependencies
session_start();
date_default_timezone_set('Australia/Melbourne');


$GLOBALS['config'] = array(
        'mysql' => array(
                /*'host' => '127.0.0.1',
                'username' => 'root',
                'password' => '',
                'db' => 'earlybirdbid'*/
                'host' => 'ecbiz187.inmotionhosting.com',
                'username' => 'akaguc5_1agu',
                'password' => 'umD[a*8UhCKW',
                'db' => 'akaguc5_kagu'

        ),
        'remember' => array(
            'cookie_name' => 'hash',
            'cookie_expiry' => '604800' //time in seconds
        ),
        'session' => array(
            'session_name' => 'user',
            'token_name'   => 'token'
        )
);

spl_autoload_register(function($class){
    require_once '../classes/'.$class.'.php';
});
require_once '../functions/sanitize.php';


if(!Input::exists()){
    echo 'No form item has been posted.';
}

include '../functions/base_url.php';

$user = new User();
$user_payments = new User_Payment();
$user_addresses = new User_Address();


if(Input::get('user')){

    $find_email = $user->findemail('email', Input::get('user'));

    if($find_email){
        $user_id    = $find_email[0]->user_id;
        $user_email = $find_email[0]->email;
    }

}else{

    $user_id    = $user->data()->user_id;
    $user_email = $user->data()->email;

}

$userdata_exist = $user_payments->find('user_payment_user_id', $user_id);

if($userdata_exist){
    Redirect::to('../index.php');
    //print_r($userdata_exist);
    //echo 'user payment details already exist.';
}


require_once('vendor/autoload.php');
// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://dashboard.stripe.com/account/apikeys
require_once ('config.php');

// Get the credit card details submitted by the form
$token = $_POST['stripeToken'];

// Create the charge on Stripe's servers - this will charge the user's card
try {
  // Create a Customer
  $customer = \Stripe\Customer::create(array(
    "source"        => $token,
    "description"   => $user_id,
    "email"         => $user_email
  ));


} catch(\Stripe\Error\Card $e) {
  // The card has been declined
  echo 'Issue with the payment';
}



$customer_json = $customer->__toJSON();
$customer_decoded = json_decode($customer_json);


$user_payments->create(array(
    'user_payment_user_id'     => $user_id,
    'user_payment_card_id'     => $customer_decoded->id,
    'user_payment_timestamp'   => time()
));


$user_addresses->create(array(
    'user_address_user_id'          => $user_id,
    'user_address_user_address'     => Input::get('address'),
    'user_address_user_suburb'      => Input::get('suburb'),
    'user_address_user_state'       => Input::get('state'),
    'user_address_user_country'     => Input::get('country'),
    'user_address_user_postal_code' => Input::get('postal_code'),
    'user_address_user_timestamp'   => time()
));



if(Input::get('page') == 'adduserdetails'){

    if(Input::get('next') == ''){

        echo '<script>window.location="../index.php?status=paymentdetailsadded";</script>';

    }else{

        echo '<script>window.location="'.Input::get('next').'&status=paymentdetailsadded";</script>';
    }
}



?>