<?php

//initialising db connection and dependencies
session_start();
date_default_timezone_set('Australia/Melbourne');


$token        = $_POST['stripeToken'];
//$productprice = $_POST['amount'];

$productprice = 10.71;
//$productprice = 1.00;

$total_1 = $productprice*100;


require_once('vendor/autoload.php');
// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://dashboard.stripe.com/account/apikeys
require_once ('config.php');

// Get the credit card details submitted by the form
//$token = $_POST['stripeToken'];

// Create the charge on Stripe's servers - this will charge the user's card
try {
  $charge = \Stripe\Charge::create(array(
    "amount" => $total_1, // amount in cents, again
    "currency" => "aud",
    "source" => $token,
    "description" => "Product Purchased: - Akagu.com.au - KHALO - Basic Contrast Tank - Grey/White XS",
    "receipt_email" => 'yingyeung86@gmail.com'
    //"receipt_email" => 'kriangsak.khanijomdi@gmail.com'
    ));
} catch(\Stripe\Error\Card $e) {
  // The card has been declined
  echo 'Issue with the payment';
}



$charge_json = $charge->__toJSON();
$charge_decoded = json_decode($charge_json);


$stored_amount = $charge_decoded->amount/100;


?>


<html>
<head>
    <title>Payment</title>
</head>
<body>
    <div style="margin: 0 auto; text-align: center;">
        <img src="../images/logo-dark-small.png" width="120"/>
        <div style="margin: 0 auto; font-size: 20px; font-family: Sans-serif; width: 40%;">
            <br/><br/>Your payment of AUD $ <?php echo $productprice ?> for <strong>KHALO - Basic Contrast Tank - Grey/White XS</strong> has been received.<br/>
            <br/>Thank you for shopping with us. :)
        </div>
    </div>
</body>
</html>
