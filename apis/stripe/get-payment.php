<?php



//initialising db connection and dependencies
session_start();
date_default_timezone_set('Australia/Melbourne');


$GLOBALS['config'] = array(
        'mysql' => array(
                /*'host' => '127.0.0.1',
                'username' => 'root',
                'password' => '',
                'db' => 'earlybirdbid'*/
                'host' => 'ecbiz187.inmotionhosting.com',
                'username' => 'akaguc5_1agu',
                'password' => 'umD[a*8UhCKW',
                'db' => 'akaguc5_kagu'

        ),
        'remember' => array(
            'cookie_name' => 'hash',
            'cookie_expiry' => '604800' //time in seconds
        ),
        'session' => array(
            'session_name' => 'user',
            'token_name'   => 'token'
        )
);

spl_autoload_register(function($class){
    require_once '../classes/'.$class.'.php';
});
    include '../functions/sanitize.php';
    include '../functions/base_url.php';
    include '../functions/current_url.php';
    include '../functions/previous_url.php';


if(!Input::exists('get')){
    echo 'No form item has been posted.';
    exit();
}

    $user = new User();
    $page = 'invoice';



  if($_GET['payment_id']){
    $id = $_GET['payment_id'];
  }else{
    echo 'No Payment ID submitted.';
    exit();
  }



require_once('vendor/autoload.php');
// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://dashboard.stripe.com/account/apikeys

require_once ('config.php');


// Create the charge on Stripe's servers - this will charge the user's card
try {
// Create a Customer
  $customer = \Stripe\Charge::retrieve($id);


} catch(\Stripe\Error\Card $e) {
  // The card has been declined
  echo 'Issue with the payment';
}



$customer_json = $customer->__toJSON();
$customer_decoded = json_decode($customer_json);

//echo $customer_decoded->id,


?>



<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Akagu | User Invoice</title>
        <meta name="description" content="Akagu curates auctions where the price drops until your desired price.">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css">
        
        
        <link rel="stylesheet" href="../css/uikit.almost-flat.css">
        <link rel="stylesheet" href="../css/child.css">
        <link rel="stylesheet" href="../css/fontello.css">

        <link rel="stylesheet" href="../css/icheck-grey.css">


        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="icon" type="image/png" sizes="32x32" href="../images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../images/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon-16x16.png">

        
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,700' rel='stylesheet' type='text/css'>



    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <main>


                <!--Page Header with Signin and SignUp buttons-->
                <!--PC Version-->
                    <div class="hidden-mb">    
                        <div class="background-dark-full">
                            
                            <div class="uk-grid">
                                <div class="uk-width-small-1-1 uk-width-medium-8-10 uk-width-large-9-10 uk-container-center uk-margin-small-top uk-margin-small-bottom" style="max-width: 1180px;">


                                    <div class="logo uk-float-left">    
                                        <a href="<?php echo base_url(TRUE).'akagu/account.php?tab=view_purchase_history'; ?>"><img src="../images/logo-light-small-notype.png" alt="" width="90" class=" uk-align-center uk-margin-bottom-remove" /></a>
                                    </div>


                                    <div class="uk-clearfix">
                                        <div class="uk-float-right">
                                            <?php
                                                if(!$user->isLoggedIn()){
                                            ?>
                                                    <a id="primary-signin-button" href="" class="link-white link-muted source_sans_pro_regular transition uk-margin-top uk-margin-right uk-margin-left" data-uk-modal="{target:'#signup'}"> SIGN IN </a>
                                                    <button id="primary-signup-button" class="call-to-action-white-outline-small source_sans_pro_regular transition uk-margin-top uk-margin-left uk-margin-small-right" data-uk-modal="{target:'#signup'}">SIGN UP</button>
                                                    <span class="uk-margin-left uk-margin-right color-smoke-grey"> | </span>

                                                    <a href="#offcanvas-menu" data-uk-offcanvas class="link-white link-muted source_sans_pro_regular uk-margin-top uk-margin-right"> <i class="icon-menu uk-icon-small"></i></a>

                                            <?php 
                                                }else{
                                            ?>
                                                <div class="uk-margin-top uk-clearfix">
                                                    <div class="uk-margin-small-top uk-clearfix">
                                                        <?php
                                                            $firstname = explode(' ', $user->data()->fullname);
                                                        ?>
                                                        <a id="primary-signin-button" href="<?php echo base_url(TRUE).'akagu_beta/account.php'; ?>" class="link-white link-muted source_sans_pro_light transition uk-margin-small-top uk-margin-small-right"> Hello, <span class="source_sans_pro_regular"><?php echo $firstname[0]; ?></span> </a>
                                                        <span class="uk-margin-left uk-margin-right color-smoke-grey"> | </span>

                                                        <a href="#offcanvas-menu" data-uk-offcanvas class="link-white link-muted source_sans_pro_regular uk-margin-top uk-margin-right"> <i class="icon-menu uk-icon-small"></i></a>
                                                    </div>
                                                </div>
                                            <?php
                                                }
                                            ?>

                                        </div>
                                    </div>

                                    <?php include_once('../templates/offcanvas-sidebar.php'); ?>

                                                                           
                                </div>

                                    

                             </div>
                         </div>

                        

                    </div>

                <section class="uk-width-medium-1-1 uk-container-center background-color-white" style="position: relative;">
                    <br/>
                
                        <div class="uk-grid">

                                <div class="uk-width-small-1-1 uk-width-medium-8-10 uk-width-large-1-1 uk-container-center uk-margin-bottom " style="max-width: 1180px;">
                                    <div class="uk-clearfix">    
                                        <div class="uk-float-left uk-margin-small-right source_sans_pro_regular color-smoke-grey" style="font-size: 13px;">    
                                            <a href="<?php echo base_url(TRUE).'akagu_beta/'; ?>" class="link-grey"><i class="uk-icon-home"></i> </a><i class="uk-icon-angle-right uk-margin-small-left uk-margin-small-right"></i><a href="" class="link-grey">   USER INVOICE  </a>
                                        </div>
                                        <div class="uk-float-right">        
                                            <a href="<?php echo base_url(TRUE).'akagu_beta/referafriend.php'; ?>" class="link-primary link-muted source_sans_pro_regular transition uk-margin-top uk-margin-right"> Refer a friend</a>
                                        </div>
                                    </div>  
                                </div>

                        </div>
                </section>



                                <section class="uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-1-1 uk-container-center uk-clearfix" style="max-width: 1180px;">

                                        <div class="uk-text-center">
                                            <h2 class="bree_bold uk-margin-top uk-margin-bottom">INVOICE</h2>
                                            <hr class="separator uk-align-center" />
                                        </div>
                                        
                                        <div class="source_sans_pro_regular uk-text-center">
                                            <p>
                                                <h4 class="uk-margin-bottom-remove source_sans_pro_regular"><strong>Your invoice for Purchase Reference Number: <?php echo $payment->id ?></strong></h4>
                                                    <div class="uk-width-small-1-1 uk-width-medium-4-10 uk-container-center uk-text-left uk-margin-top uk-text-la">
                                                        <div class="uk-grid">
                                                           <div class="uk-width-small-1-1 uk-width-medium-1-2">
                                                                <div>Purchase Invoice Number: </div>
                                                                <div>Purchase Status: </div>
                                                                <div>Purchase Total Amount: </div>
                                                                <div>Purchase Subtotal: </div>
                                                                <div>Purchase Tax: </div>
                                                                <div>Purchase Shipping: </div>
                                                                <div>Purchase Item Description: </div>

                                                           </div>
                                                           <div class="uk-width-small-1-1 uk-width-medium-1-2">
                                                                <div><?php echo strtoupper($customer_decoded->id);  ?></div>
                                                                <div><?php echo ucfirst($customer_decoded->status); ?></div>
                                                                <div><?php echo strtoupper($customer_decoded->currency).' '.sprintf("%0.2f", $customer_decoded->amount/100); ?></div>

                                                                <?php 
                                                                  $gst = (1/11) * ($customer_decoded->amount/100);
                                                                  $subtotal = ($customer_decoded->amount/100) - $gst;
                                                                ?>

                                                                <div><?php echo strtoupper($customer_decoded->currency).' '.sprintf("%0.2f", $subtotal); ?></div>
                                                                <div><?php echo strtoupper($customer_decoded->currency).' '.sprintf("%0.2f", $gst); ?></div>
                                                                <div><?php echo strtoupper($customer_decoded->currency).' '.sprintf("%0.2f", 00); ?></div>
                                                                <div><?php echo $customer_decoded->description; ?></div>
                                                           </div>



                                                        </div>
                                                    </div>
                                                   
                                                <hr class="separator uk-align-center" />
                                                <div class="uk-text-center source_sans_pro_regular">
                                                    <a href="<?php echo '../upcoming-events.php'; ?>" class="uk-link uk-margin-right">Continue shopping by clicking here</a> | <a href="../account.php?tab=view_purchase_history" class="uk-link uk-margin-left">View Purchase History</a>
                                                </div>
                                            </p>
                                            
                                        </div>
                                        
                                </section>

                <hr class="separator uk-align-center" />
                 <!-- Upcoming Auctions -->
                <?php //include_once('templates/upcoming-auctions.php'); ?>



                <!-- CTA Member Subscription -->
                <?php include_once('../templates/member-subscription.php'); ?>



                <!--Footer Row 1 (Secondary Navigation)-->
                <?php include_once('../templates/footer-row-1.php'); ?>


                
                <!--Section 6 (Supplier's Contact Form)-->
                <?php include_once('../templates/supplier-contact-form.php'); ?>


                    
                <!--Footer Row 2 (Copyright and Social Links)-->
                <?php include_once('../templates/footer-row-2.php'); ?>

                <a name="footer"></a>
            </div>


                <!-- Vimeo Modal -->
                <?php include_once('../templates/modal-vimeo.php') ?>



                <!-- Signup Modal -->
                <?php include_once('../templates/modal-member.php') ?>



                <!-- Subscribe Modal -->
                <?php include_once('../templates/modal-subscribe.php') ?>



        </main>



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="../js/vendor/modernizr-2.8.3.min.js"></script>
        
        <script src="../js/uikit.js"></script>
        <script src="../js/components/slider.js"></script>

        <script src="../main.js"></script>
        <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
        <script src="../js/validations/validate-member-signin-form.js"></script>
        <script src="../js/validations/validate-member-signup-form.js"></script>

        <script src="../js/icheck.js"></script>
        
    
        <script>/*
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-60854610-1', 'auto');
          ga('send', 'pageview');*/

        </script>

    </body>
</html>
