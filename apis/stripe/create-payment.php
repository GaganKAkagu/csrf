<?php

//initialising db connection and dependencies
session_start();
date_default_timezone_set('Australia/Melbourne');


$GLOBALS['config'] = array(
        'mysql' => array(
                /*'host' => '127.0.0.1',
                'username' => 'root',
                'password' => '',
                'db' => 'earlybirdbid'*/
                'host' => 'ecbiz187.inmotionhosting.com',
                'username' => 'akaguc5_1agu',
                'password' => 'umD[a*8UhCKW',
                'db' => 'akaguc5_kagu'

        ),
        'remember' => array(
            'cookie_name' => 'hash',
            'cookie_expiry' => '604800' //time in seconds
        ),
        'session' => array(
            'session_name' => 'user',
            'token_name'   => 'token'
        )
);

spl_autoload_register(function($class){
    require_once '../classes/'.$class.'.php';
});
require_once '../functions/sanitize.php';


if(!Input::exists('get')){
    echo 'No form item has been posted.';
    exit();
}

include '../functions/base_url.php';
include '../functions/getpriceandtime-v2.php';
include '../analytics.js';


$user = new User();
$user_payments = new User_Payment();
$product = new Product();
$purchases = new Purchase_Detail();
$coupons = new Coupon();




$product_details = $product->find('product_id', Input::get('product_id'));
$card_token_details = $user_payments->find('user_payment_user_id', $user->data()->user_id);
$product_option_get = $_SESSION['options'];

$product_options = new Product_Option();
$option = $product_options->find('product_options_id', (int)$product_option_get);

$product_option = $option[0]->product_options_product_option;
$option_id = $option[0]->product_options_id;



$productprice = getprice($product_details[0]->product_bid_start_date, $product_details[0]->product_bid_start_time, $product_details[0]->product_bid_end_date, $product_details[0]->product_bid_end_time, $product_details[0]->product_price_start, $product_details[0]->product_price_end, $product_details[0]->product_drop_frequency);


$find_coupon = $coupons->find('coupon_code', Input::get('coupon_code'));
    
if($find_coupon){
    if($find_coupon[0]->coupon_usage_count > 0){            
        $cal = $productprice - ($productprice * ($find_coupon[0]->coupon_value/100));
        $subtotal = $cal;
        $coupon_code = Input::get('coupon_code');
    }
}else{
    $subtotal = $productprice;

}


$gst = (1/11) * $subtotal;
$delivery = $product_details[0]->product_delivery_cost;

$gst = (1/11) * $subtotal;
$actual_price = $subtotal - $gst;
$total = $actual_price + $gst + $delivery;

$total_1 = round($total, 2)*100;

require_once('vendor/autoload.php');
// Set your secret key: remember to change this to your live secret key in production
// See your keys here https://dashboard.stripe.com/account/apikeys
require_once ('config.php');

// Get the credit card details submitted by the form
//$token = $_POST['stripeToken'];

// Create the charge on Stripe's servers - this will charge the user's card
try {
  $charge = \Stripe\Charge::create(array(
    "amount" => $total_1, // amount in cents, again
    "currency" => "aud",
    "customer" => $card_token_details[0]->user_payment_card_id,
    "description" => "Product Purchased: - Akagu.com.au - ".ucwords($product_details[0]->product_name)." #".$product_details[0]->product_id." - Option: ".$product_option,
    "receipt_email" => $user->data()->email
    ));
} catch(\Stripe\Error\Card $e) {
  // The card has been declined
  echo 'Issue with the payment';
}


    $available_quantity = $option[0]->product_options_product_quantity;

    $product_quantity = (int)$available_quantity - 1;

    $product_options->update('product_options', array(
        'product_options_product_quantity' => (int)$product_quantity,
    ), $option_id);

    
if($find_coupon){
    $available_coupon = $find_coupon[0]->coupon_usage_count;

    $coupon_quantity = (int)$available_coupon - 1;

    $coupons->update('coupons', array(
        'coupon_usage_count' => (int)$coupon_quantity,
    ), $find_coupon[0]->coupon_id);
}



$charge_json = $charge->__toJSON();
$charge_decoded = json_decode($charge_json);

?>

<script>
    ga('ec:addProduct', {                                                // Provide product details in an productFieldObject.
      'id': <?php echo $product_details[0]->product_id; ?>,             // Product ID (string).
      'name': <?php echo ucwords($product_details[0]->product_name); ?>, // Product name (string).
      'variant': <?php echo $product_option; ?>,                         // Product variant (string).
      'price': <?php echo $total_1/100; ?>,                              // Product price (currency).
      'coupon': <?php echo $coupon_code; ?>,                             // Product coupon (string).
      'quantity': 1                                                      // Product quantity (number).
    });

    ga('ec:setAction','checkout', {
        'step': 3,            // A value of 1 indicates this action is first checkout step.
        'option': 'Create Payment'      // Used to specify additional info about a checkout stage, e.g. payment method.
    });

    ga('ec:setAction', 'purchase', {                                     // Transaction details are provided in an actionFieldObject.
      'id': <?php echo $charge_decoded->id; ?>,             // (Required) Transaction id (string).
      'revenue': <?php echo $charge_decoded->amount/100; ?>,                                // Revenue (currency).
      'tax': <?php echo $gst; ?>,                                        // Tax (currency).
      'shipping': <?php echo $delivery ?>,                               // Shipping (currency).
      'coupon': <?php echo $coupon_code; ?>                              // Transaction coupon (string).
    });

    ga('send', 'pageview');     // Send transaction data with initial pageview.
</script>

<?php

$_SESSION['payment']['reference']   = $charge_decoded->id;
$_SESSION['payment']['status']      = $charge_decoded->status;
$_SESSION['payment']['total']       = $charge_decoded->amount;
$_SESSION['payment']['description'] = $charge_decoded->description;



$stored_amount = $charge_decoded->amount/100;


$purchases->create(array(
    'purchase_user_id'          => $user->data()->user_id,
    'purchase_product_id'       => $product_details[0]->product_id,
    'purchase_reference_number' => $charge_decoded->id,
    'purchase_status'           => $charge_decoded->status,
    'purchase_currency'         => $charge_decoded->currency,
    'purchase_subtotal'         => $stored_amount,
    'purchase_shipping'         => $product_details[0]->product_delivery_cost,
    'purchase_tax'              => $gst,
    'purchase_description'      => $charge_decoded->description,
    'purchase_invoice_number'   => $charge_decoded->id,
    'purchase_timestamp'        => time()
));


echo '<script>window.location="../purchase-confirmation.php"</script>';


?>