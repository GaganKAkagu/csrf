<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Paypal Integration Test - Live Database Connection</title>
</head>
<body>

<form class="paypal" action="listener_db_formatted.php" method="post" id="paypal_form" target="_blank">

        <input type="hidden" name="cmd" value="_xclick" />

       <!-- Specify details about the item that buyers will purchase. -->
        
        <input type="hidden" name="currency_code" value="USD">

        <!--instruct paypal to post its own parameters as POST and keep your parameters as GET on the successReport.php-->
        <!--“a value of 2 means: the payer’s browser is redirected to the return URL by the POST method, and all transaction variables are also posted”-->
        <input type='hidden' name='rm' value='2'>
        
        <!-- Display the payment button. -->
        <input type="image" name="submit" border="0"
        src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif" alt="PayPal - The safer, easier way to pay online">
        <img alt="" border="0" width="1" height="1" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

        
    </form>

</body>
</html>


