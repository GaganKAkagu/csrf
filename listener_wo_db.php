// without remote database connection

<?php

//Database Connection
//require_once('core/init.php');

//Calling the get price function (located in the same directory)
//require_once ('get-price-and-time.php');


// PayPal settings
$paypal_email = 'akagu.business@test.com'; // Akagu Paypal Business email
$return_url = '/success.html';
$cancel_url = '/cancel.php';
$notify_url = 'http://akagustaging.ap-southeast-2.elasticbeanstalk.com/listener.php';

//$users        		= new User();
//$user_payments   	= new User_Payment();
//$user_addresses   	= new User_Address();
//$products 			= new Product();
//$brands 			= new Brand();
//$product_options 	= new Product_Option();
//$purchases 			= new Purchase_Detail();
//$coupons 			= new Coupon();

//$check_user_payment = $user_payments->find('user_payment_user_id', $users->data()->user_id);
//$user_address     = $user_addresses->find('user_address_user_id', $users->data()->user_id);

//define all variables to local variables
		$delivery_option 	= 7;
		//$coupon_code       	= Input::get('coupon_code');

		//$product_id 		= $_SESSION['product_id'];
		//$product_option 	= $_SESSION['product_option'];
		$product_quantity 	= 1;
		$action             = 'buy';
		//$time 				= $_SESSION['time'];
		$product_name = 'Bluegrass Wide Leg Pant';
		$product_id = '87';

//Product Price is the actual product price obtained by using the price drop algorithm
//Subtotal represents the product price after a coupon code has been applied if available


//fetching product data and product price when a user clicks on the bid now button
		//$product 		= $products->find('product_id', $product_id);

//fetching product's brand name
    //$brand_name             = $brands->find('brand_id', $product[0]->product_brand_id);
		
		/*if($action == 'bid'){
            $product_price  = getprice($product[0]->product_bid_start_date, $product[0]->product_bid_start_time, $product[0]->product_bid_end_date, $product[0]->product_bid_end_time, $product[0]->product_price_start, $product[0]->product_price_end, $product[0]->product_drop_frequency, $time);//get product price using timer input
        }else if($action == 'buy'){
            $product_price  = $product[0]->product_rrp;//get product price using timer input
        }

        //calculating the product price with the quantity accounted for
		$product_price  = $product_price * $product_quantity;*/


// Check if paypal request or response
if (!isset($_POST["txn_id"]) && !isset($_POST["txn_type"])){
    $querystring = '';
    
    // Firstly Append paypal account to querystring
    $querystring .= "?business=".urlencode($paypal_email)."&";
    
    // Append amount&amp;amp;amp; currency (£) to quersytring so it cannot be edited in html
    
    //The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
    //$querystring .= "item_name=".urlencode($product_name)."&";
    //$querystring .= "amount=".urlencode($product_price)."&";
    //$querystring .= "item_number=".urlencode($product_id)."&";
    
    //loop for posted values and append to querystring
    foreach($_POST as $key => $value){
        $value = urlencode(stripslashes($value));
        $querystring .= "$key=$value&";
    }
    
    // Append paypal return addresses
    $querystring .= "return=".urlencode(stripslashes($return_url))."&";
    $querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
    $querystring .= "notify_url=".urlencode($notify_url);
    
    // Append querystring with custom field
    //$querystring .= "&amp;amp;amp;custom=".USERID;
    
    // Redirect to paypal IPN
    header('location:https://www.sandbox.paypal.com/cgi-bin/webscr'.$querystring);
    exit();
} else {
    
    // Response from Paypal

    // read the post from PayPal system and add 'cmd'
    $req = 'cmd=_notify-validate';
    foreach ($_POST as $key => $value) {
        $value = urlencode(stripslashes($value));
        $value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i','${1}%0D%0A${3}',$value);// IPN fix
        $req .= "&$key=$value";
    }
        
    // post back to PayPal system to validate
    $header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
    $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
    $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
    
    $fp = fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
    
    if (!$fp) {
        // HTTP ERROR
        
    } else {
        fputs($fp, $header . $req);
        while (!feof($fp)) {
            $res = fgets ($fp, 1024);
            if (strcmp($res, "VERIFIED") == 0) {

              $arr=array();

            	$valid_price= false;

            	// assign posted variables to local variables
					$data['item_name'] = $_POST['item_name'];
					$data['item_number'] = $_POST['item_number'];
					$data['payment_status'] = $_POST['payment_status'];
					$data['payment_amount'] = $_POST['mc_gross'];
					$data['payment_currency'] = $_POST['mc_currency'];
					$data['txn_id'] = $_POST['txn_id'];
					$data['receiver_email'] = $_POST['receiver_email'];
					$data['payer_email'] = $_POST['payer_email'];
					//$quantity = $_POST['quantity'];

          //basic math calculation - final product price
          $gst  = (1/11) * $data['payment_amount'];
          $price  = $data['payment_amount'] - $gst;
          $total  = $price + $gst + $delivery_option;
                

                array_push($arr, array(

               "purchase_product_id" => $data['item_number'], 
               "purchase_quantity" => $data['quantity'],
               "purchase_reference_number" => $data['txn_id'],
               "purchase_status" => $data['payment_status'],
               "purchase_currency" => $data['payment_currency'],
        "payment_amount" => $data['payment_amount'],
        "purchase_description" => $data['item_name'],
        "gst" => $gst,
        "shipping" => $delivery_option,
        "total" => $total,
        "net price" => $price,
        //"paypal_fee" => $paypal_payment_fee,
        //"payment_method" => "PayPal Payment"

           ));
           

       if($arr){
           $fp = fopen('listener-result.json', 'w');
           fwrite($fp, json_encode($arr));
           fclose($fp);
       }
                // Used for debugging
                // mail('user@domain.com', 'PAYPAL POST - VERIFIED RESPONSE', print_r($post, true));
                        
                // Validate payment (Check correct price)
                //$valid_price = check_price($data['payment_amount'], $data['item_number']);
				/*if($data['payment_amount']==$product_price && $data['item_number']==$product_id){
					$valid_price = true;
				}else{
					$valid_price = false;
				}	

                // Payment Validated
                if ($valid_price) {
                    
                    //$orderid = updatePayments($data);

                	//basic math calculation - final product price
					$gst 	= (1/11) * $data['payment_amount'];
					$price  = $data['payment_amount'] - $gst;
					$total  = $price + $gst + $delivery_option;

                    //save purchase details
		        	try{
		        		
						$purchases->create(array(
						    //'purchase_user_id'          => $users->data()->user_id,
						    'purchase_product_id'       => $product[0]->product_id,
						    'purchase_quantity'			=> $product_quantity,
						    'purchase_reference_number' => $data['txn_id'],
						    'purchase_status'           => $data['payment_status'],
						    'purchase_currency'         => $data['payment_currency'],
						    'purchase_subtotal'         => $data['payment_amount'],
						    'purchase_shipping'         => $delivery_option,
						    'purchase_tax'              => $gst,
						    //'purchase_description'      => $charge_decoded->description,
						    'purchase_invoice_number'   => $data['txn_id'],
						    'purchase_timestamp'        => date("D, d M Y H:i:s")
						));

		        	} catch (Exception $e){
		        		die($e->getMessage());
		        	}
                    
                    
                } else {
                  // Error inserting into DB
                        // E-mail admin or alert user
                        // mail('user@domain.com', 'PAYPAL POST - INSERT INTO DB WENT WRONG', print_r($data, true));
                    
                    // E-mail admin or alert user
                }
            */
            } else if (strcmp ($res, "INVALID") == 0) {
            
                // PAYMENT INVALID &amp;amp;amp; INVESTIGATE MANUALY!
                // E-mail admin or alert user
                
                // Used for debugging
                //@mail("user@domain.com", "PAYPAL DEBUGGING", "Invalid Response

                // IPN invalid, log for manual investigation
			    echo "The response from IPN was: <b>" .$res ."</b>";

				// log for manual investigation
				// Add business logic here which deals with invalid IPN messages
				if(DEBUG == true) {
					error_log(print_r(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE));
				}


            }
        }
    fclose ($fp);
    }

}






?>