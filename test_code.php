<?php

// Database variables
$servername = "aa1usev0ag4x0kl.cyr0p8tmvp7b.ap-southeast-2.rds.amazonaws.com"; //database location
$user = "akaguc51agu"; //database username
$pass = "umDa8UhCKW"; //database password
$db_name = "akagu_test"; //database name



// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
// Set this to 0 once you go live or don't require logging.
define("DEBUG", 1);

// Set to 0 once you're ready to go live
define("USE_SANDBOX", 1);


define("LOG_FILE", "./ipn.log");

// Read POST data
// reading posted data directly from $_POST causes serialization
// issues with array data in POST. Reading raw POST data from input stream instead.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);

$myPost = array();
foreach ($raw_post_array as $keyval) {
	$keyval = explode ('=', $keyval);
	if (count($keyval) == 2)
		$myPost[$keyval[0]] = urldecode($keyval[1]);
}


// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
	$get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
	if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
		$value = urlencode(stripslashes($value));
	} else {
		$value = urlencode($value);
	}
	$req .= "&$key=$value";
}

// Post IPN data back to PayPal to validate the IPN data is genuine
// Without this step anyone can fake IPN data

if(USE_SANDBOX == true) {
	$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
} else {
	$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
}

$ch = curl_init($paypal_url);
if ($ch == FALSE) {
	return FALSE;
}

curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);



// CONFIG: Optional proxy configuration
//curl_setopt($ch, CURLOPT_PROXY, $proxy);
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);

// Set TCP timeout to 30 seconds
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
// of the certificate as shown below. Ensure the file is readable by the webserver.
// This is mandatory for some environments.

//$cert = __DIR__ . "./cacert.pem";
//curl_setopt($ch, CURLOPT_CAINFO, $cert);

$res = curl_exec($ch);
if (curl_errno($ch) != 0) // cURL error
	{
	if(DEBUG == true) {	
		error_log(print_r(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE));
	}
	curl_close($ch);
	exit;

} else {
		// Log the entire HTTP response if debug is switched on.
		if(DEBUG == true) {
			error_log(print_r(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE));
			error_log(print_r(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE));
		}
		curl_close($ch);
}

// Inspect IPN validation result and act accordingly

// Split response headers and payload, a better way for strcmp
$tokens = explode("\r\n\r\n", trim($res));
$res = trim(end($tokens));

if (strcmp ($res, "VERIFIED") == 0) {
	// check whether the payment_status is Completed
	// check that txn_id has not been previously processed
	// check that receiver_email is your PayPal email
	// check that payment_amount/payment_currency are correct
	// process payment and mark item as paid.	

	// assign posted variables to local variables
	$item_name = $_POST['item_name'];
	$item_number = $_POST['item_number'];
	$payment_status = $_POST['payment_status'];
	$payment_amount = $_POST['mc_gross'];
	$payment_currency = $_POST['mc_currency'];
	$txn_id = $_POST['txn_id'];
	$receiver_email = $_POST['receiver_email'];
	$payer_email = $_POST['payer_email'];
	$quantity = $_POST['quantity'];
	$paypal_payment_fee = $_POST['mc_fee'];
	$shipping = $_POST['shipping'];
	$payment_date = $_POST['payment_date'];

	//Change $payment_date value from Paypal Sandbox(PDT - PDT in the summer and PST in the winter) 
	//to AEST
	$time_object = new DateTime($payment_date, new DateTimeZone('PDT'));
	$time_object->setTimezone(new DateTimeZone('Australia/Melbourne'));
	$AustraliaDateTime = $time_object->format('D, d M Y H:i:s');
	//echo $AustraliaDateTime;	
	

    //basic math calculation - final product price
    $item_price = $payment_amount - $shipping;
    $gst  = (1/11) * $item_price;
    $net_price  = $item_price - $gst;
    $total_price  = $net_price + $gst + $shipping;

    // Open Database Connection
	$conn = new PDO("mysql:host=$servername;dbname=$db_name", $user, $pass);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Database connection
	$stmt = $conn->prepare("SELECT * FROM products WHERE product_id = " . $item_number); 
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_OBJ);

    //calculating the product price with the quantity accounted for
    $product_price = $result->product_rrp;
    $product_price  = $product_price * $quantity;
    $local_currency = "AUD";


    // checking values in json file as well
    $arr = array();
    array_push($arr, array(

               "purchase_product_id" => $item_number, 
               "purchase_quantity" => $quantity,
               "purchase_reference_number" => $txn_id,
               "purchase_status" => $payment_status,
               "purchase_currency" => $payment_currency,
				"total_amount" => $payment_amount,
				"purchase_description" => $item_name,
				"gst(tax)" => $gst,
				"net_price" => $net_price,
				"shipping" => $delivery_option,
				"paypal_fee" => $paypal_payment_fee,
				"payment_method" => "PayPal Payment",
				"shipping" => $shipping,
				"payment_date" => $AustraliaDateTime,
				"product_price" => $product_price,
				"item_price" => $item_price

           ));
           

       if($arr){
           $fp = fopen('listener-result.json', 'w');
           fwrite($fp, json_encode($arr));
           fclose($fp);
       }

       //Validation : check that item_price ($payment_price - $shipping) & payment_currency are correct
       if($payment_currency == $local_currency && $item_price == $product_price){

	       //save purchase details
		    try{
		    		// exclude product_id as it's an auto-incremental field
		    		// use _Session_userID for purchase_user_id on production site

		            $statement = $conn->prepare("INSERT INTO purchase_details(purchase_user_id, purchase_product_id,purchase_quantity, purchase_reference_number,
	                                purchase_status, purchase_currency, purchase_method, 
	                                purchase_fee, purchase_subtotal,
	                                purchase_shipping, purchase_tax, purchase_description,
	                                purchase_invoice_number, purchase_timestamp)
		            VALUES(:purchase_user_id, :purchase_product_id,:purchase_quantity, :purchase_reference_number,
	                                :purchase_status, :purchase_currency, :purchase_method, 
	                                :purchase_fee, :purchase_subtotal,
	                                :purchase_shipping, :purchase_tax, :purchase_description,
	                                :purchase_reference_number, :purchase_timestamp)");

	        $statement->execute(array(

	                "purchase_user_id" => 27,
	                "purchase_product_id" => $item_number, 
	                "purchase_quantity" => $quantity,
	                "purchase_reference_number" => $txn_id,
	                "purchase_status" => $payment_status,
	                "purchase_currency" => $payment_currency,
	                "purchase_method" => "PayPal",
	                "purchase_fee" => $paypal_payment_fee,
	                "purchase_subtotal" => $payment_amount,
	                "purchase_description" => $item_name,
	                "purchase_tax" => $gst,
	                "purchase_shipping" => $shipping,
	                "purchase_timestamp" => $AustraliaDateTime
	        ));

		    }catch (Exception $e){
		         // Exception error 
		    }

		}else{
			// Error Handling
			// if the total amount of item(s) != amount of item(s) coming back from Paypal
			// Add business logic
			// Email Akagu admin and cutomer for IPN fraud
			
		}	
	
	
} else if (strcmp ($res, "INVALID") == 0) {

	// IPN invalid, log for manual investigation
    echo "The response from IPN was: <b>" .$res ."</b>";

	// log for manual investigation
	// Add business logic here which deals with invalid IPN messages
	
}

?>